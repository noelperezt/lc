#!/usr/bin/env python
import sys
import os
from os import listdir
from os.path import join

import compileall


compileall.compile_dir('apps', legacy=True)
compileall.compile_dir('LC', legacy=True)


dirs = ["apps", "LC"]

def scandirs(path, exts):
    for root, dirs, files in os.walk(path):
        for currentFile in files:
            if any(currentFile.lower().endswith(ext) for ext in exts):
                os.remove(os.path.join(root, currentFile))


for dir in dirs:
    scandirs(dir, ('.py',))