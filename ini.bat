@echo off

echo "eliminando migraciones"
@RD /S /Q "apps/administrador/migrations"
@RD /S /Q "apps/alojamiento/migrations"
@RD /S /Q "apps/institucion/migrations"

echo "Creando migraciones."
python manage.py makemigrations administrador institucion alojamiento

echo "Migrando..."
python manage.py migrate

python manage.py loaddata initial_data.json
::python manage.py loaddata ciudades.json
::python manage.py runserver