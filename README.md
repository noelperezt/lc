# LC

Instalación de dependencia

* pip install -r Requirements.txt

Migraciones

* python manage.py makemigrations administrador institucion alojamiento
* python manage.py migrate

Creacion de super usuario

* python manage.py createsuperuser --email=admin@lc.com

Crear la data inicial

* python manage.py dumpdata --format=json --indent 4 administrador --exclude administrador.ciudades > apps/administrador/fixtures/initial_data.json
* python manage.py dumpdata --format=json --indent 4 administrador.ciudades > apps/administrador/fixtures/ciudades.json

Ejecutar la información inicial

* python manage.py loaddata initial_data.json
* python manage.py loaddata ciudades.json

Usuario inicial
* Usuario: admin@lc.com
* Password: 1234qwer

Dependencias:

* [Django](http://www.djangoproject.com/) >= 1.9
* [dateutil](http://labix.org/python-dateutil) library for flexible, fault-tolerant date parsing.

# Caracteristicas

* ...
* ...
* ...

# Dirección
[LC](http://www.lc.com/)
