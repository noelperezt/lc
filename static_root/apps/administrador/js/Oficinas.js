var aplicacion, $form, tabla = false, to, $arbol, clon = 0, clonImg = 0;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;
		},
		'limpiar' : function(){
			$(".trTlf[clon!=0]", "#tblTlfs").detach();
			$(".ItemMultimedia[clon!=0]", "#imagenes").detach();
			tabla.fnDraw();

		},
		'buscar' : function(r){
			alert("buscar");
			$(".selectpicker").selectpicker('refresh');
			$("#id_iframe_ubicacion").val(r.iframe_ubicacion);
		},
		'guardar' : function(r){ 
			if (r.email != undefined)
				aviso(r.email);
		}
	});

	$("#id_pais").change(function(){
		aplicacion.selectCascada($(this).val(), "#id_ciudad", 'ciudades', "refreshSelectpickers");
	});

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$form = aplicacion.form;

	$(".btnCargaImg").click(function(){
		inputfile = $(this).attr("inputfile");
		$("#"+inputfile).trigger("click");
	});

	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});

	$('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	/* TAB OFICINAS */
	$("#tlf_oficina", "#fOficinas").mask("0999-999-9999");

	$(".btnAddTlf").click(function(){
		AddTlf("");
	});

	$(".btnAddImg").click(function(){
		AddImg("", "");
	});	
});

function AddTlf($nro){
	clon++;
	$(".trTlf[clon=0]", "#tblTlfs").clone().attr("clon", clon).appendTo("#tblTlfs");

	$("input[name='telefono[]']", ".trTlf[clon="+ clon +"]").val($nro);

	$(".trTlf[clon!=0] .fa-minus", "#tblTlfs").click(function(){
		$(this) .parents(".trTlf").detach();
	});
}

function AddImg($ruta_archivo, $texto){
	alert("addimg");
	clonImg++;
	$(".trImg[clon=0]", "#tblImgs").clone().attr("clon", clonImg).appendTo("#tblImgs");
	
	$tr = $(".trImg[clon="+clonImg+"]", "#tblImgs");

	$("input[name='txtimg[]']", $tr).val($texto);
	$("input[name='archivo[]']", $tr).val($ruta_archivo);
	$("img", $tr).attr("src", $ruta_archivo);

	if($ruta_archivo == '')
		$("input[name='fileimg[]']", $tr).trigger("click");
}

function actualizaImg(input){
	$tr = $(input).parents(".trImg");
	$("input[name='archivo[]']", $tr).val($(input).val());
	previewImg(input, ".trImg[clon=" + $tr.attr("clon") + "] td img");
}

function clickImg(img){
	$tr = $(img).parents(".trImg");
	$("input[name='fileimg[]']", $tr).trigger("click");
}

function borraImg(img){
	$(img).parents(".trImg").detach();
}