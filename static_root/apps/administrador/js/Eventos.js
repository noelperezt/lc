var aplicacion, $form, tabla = false, to, $arbol, clonPlan = 0;

$(function() {
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

            $("#id_descripcion").val(tinymce.get('id_descripcion').getContent());
            return true;
        },
        'limpiar' : function(){
            tabla.fnDraw();
            $(".previewimg").prop("src", '');
            $(".contenedorMultimedia").html("");
        },
        'buscar' : function(r){
            $("#foto").prop("src", r.archivo);
            tinymce.get('id_descripcion').setContent(r.descripcion);
        }
    });

    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <i class="fa fa-pencil" aria-hidden="true"></i>\
                <i class="fa fa-trash" aria-hidden="true"></i>\
            '
        } ]
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('.datatable').on('click', '.fa-pencil', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.fa-trash', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });
    $('#tiposLc').on('click', '.fa-save', function (e) {
        var nombre = $("#nombre_tipo_servicio").val();
        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_tipo/',
            data: {
                'nombre': nombre,
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (data) {
                $(".inputguardar").val('');
                
            }
        });
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $(".inputfile").change(function(){
        previewImg(this, $(this).attr("destinopreview"));
    });
    
    $(".btnCargaImg").click(function(){
        inputfile = $(this).attr("inputfile");
        $("input[name='" + inputfile + "']").trigger("click");
    });



    $("#id_pais").change(function(){
        aplicacion.selectCascada($(this).val(), "#id_ciudad", 'ciudades', "refreshSelectpickers");
    });

    $("#btn_add_plan").click(function(){
        $("#modalPlan").modal("show");
    });

    $("#btnGuardarplan").click(function(){
        if($.trim($("#nombre_plan", "#modalPlan").val()) == '' || $.trim($("#precio_plan", "#modalPlan").val()) == '')
            return false;

        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_plan/',
            data: {
                'nombre_plan': $("#nombre_plan", "#modalPlan").val(),
                'precio_plan': $("#precio_plan", "#modalPlan").val(),
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (r) {
                aviso(r.msj);

                if(r.s == 's'){
                    addTrPlan($nombre, $precio);
                    $("#modalPlan").modal("hide");
                }                
            }
        });
    });

    $("#guardarFechaInicio").datepicker({
        format: 'dd/mm/yyyy',
        language : 'es'
    });.
});

function addTrPlan($id_plan, $nombre_plan, $precio_plan){
    clonPlan++;
    $(".trPlan[clon=0]").clone().attr("clon", clonPlan).appendTo("#tbl_planes");

    $("input[name='id_plan[]']", ".trPlan[clon="+ clonPlan +"]").val($id_plan);
    $(".td_nombre_plan", ".trPlan[clon="+ clonPlan +"]").html($nombre_plan);
    $(".td_precio_plan", ".trPlan[clon="+ clonPlan +"]").html($precio_plan);
}