var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

        },
        'limpiar' : function(){
            tabla.fnDraw();
        },
        'buscar' : function(r){
           
        }
    });

    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <button class="btn btn-primary">Editar</button>\
                <button class="btn btn-danger">Eliminar</button>\
            '
        } ]
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('.datatable').on('click', '.btn-primary', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btn-danger', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $("#btnCargaImg").click(function(){
        $("#fotousuario").trigger("click");
    });

    $("", "#contUbicacion").change(function(){
        // hace#id_paisr ajax para buscar ciudades
    });

    $("#id_pais", "form").keypress(function(event){
        if (event.which == 13)
            if($.trim($(this).val()) == '')
                return false;

        // hacer ajax para guardar
    }).change(function(){
        buscarCiudades(this, $("#id_ciudad", "form"));
    });


});