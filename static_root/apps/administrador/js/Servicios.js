var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

            if($(".flat:checked").length < 1){
                aviso("Seleccione un tipo de servicio");
                return false;                
            }else if($(".flat:checked").length > 1){
                aviso("Seleccione un solo tipo de servicio");
                return false;                
            }

            $("#id_descripcion").val(tinymce.get('id_descripcion').getContent());
            return true;
        },
        'limpiar' : function(){
            tabla.fnDraw();
            $("#foto").prop("src", '');
        },
        'buscar' : function(r){
            $("#foto").prop("src", r.archivo);
            tinymce.get('id_descripcion').setContent(r.descripcion);

            $.each(r.owner, function(ll, v){
                alert(v);
                $(".flat[value='" + v + "']").prop("checked", "checked");
            });

            $('input.flat').iCheck({
               checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        }
    });

    $("#id_descripcion").removeAttr('required');
    
    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <div class="btnaccion btnaccionedita"></div>\
                <div class="btnaccion btnaccioneliminar"></div>\
            '
        } ]
    });

    $('.datatable').on('click', '.btnaccionedita', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $("#nombre_tipo_servicio").keypress(function(event){
        if(event.which == 13)
            $('#tiposLc').trigger("click");
    });

    $('#tiposLc').on('click', '.fa-save', function (e) {
        var nombre = $("#nombre_tipo_servicio").val();
        if($.trim(nombre) == '')
            return false;

        $existe = false;
        $(".tdServicio").each(function(){
            if($.trim(nombre.toUpperCase()) == $.trim($(this).html().toUpperCase()))
                $existe = true;
        });
        
        if ($existe == true) {
            aviso("Tipo de servicio ya existe en la lista");
            return false;
        };

        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_tipo/',
            data: {
                'nombre': nombre,
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (data) {
                $(".inputguardar").val('');
                $('.table', ".contenedortituloselect").append('<tr>\
                    <td width="30px"><input type="checkbox" class="flat" name="owner" value="' + data.id + '"></td>\
                    <td class="tdServicio"> ' + data.nombre + ' </td>\
                </tr>');

                $('input.flat').iCheck({
                   checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            }
        });
    });
    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $("#id_archivo").change(function(){
        previewImg(this, '.fotopreview');
    });
    
    $(".btnCargaImg").click(function(){
        $("#id_archivo").trigger("click");
    });
});