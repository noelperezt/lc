var aplicacion, $form, tabla = false, to;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'limpiar' : function(){
			$("div.descripcion", "#formulario").html("");
			tabla.fnDraw();
		},
		'buscar'	: function(r){
			$.each(r, function(ll, v){				
				$(".info .descripcion[campo='" + ll + "']", "#formulario").html(v);	
			});
			
			$("#myModal").modal("show");
		}
	});

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$form = aplicacion.form;

	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionresponder"></div>\
				<div class="btnaccion btnaccionver"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});

	$('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

    $("#btnPdf").click(function(){
    	if ($(this).val() == '#')
    		return false;

    	window.open($(this).attr("rutapdf"));
    });
});