var aplicacion, $form, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit': function(e) {
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes': function(accion) {
            if (accion !== 'guardar') return;
            $("#estructura").val(get_data_arbol());
        },
        'limpiar': function() {

        },
        'buscar': function(r) {

        }
    });

    $form = aplicacion.form;

    $("#guardar").click(function() {
        aplicacion.guardar();
    });
    $("#limpiar").click(function() {
        aplicacion.limpiar();
    });

    arbol();

    $('#id_nombre').keypress(function(e) {
        if (e.which == 13) {
            e.preventDefault();
            add_arbol(['#']);
        }
    });


    $('#limpiar').click(function(e) {
        $('#id_nombre').val('');
        arbol();
    });

    $('#deseleccionar').click(function(e) {
        $arbol.deselect_all();
    });
});

function get_data_arbol() {
    return JSON.stringify($arbol.get_json('#', { 'flat': true }));
}

function add_arbol(parent) {
    var position = 'inside';
    var parent = parent || $arbol.get_selected();
    var newNode = {
        'state': {
            'opened': true
        },
        'text': $('#id_nombre').val(),
        'data': {
            'data': 'mydata'
        }
    };

    $('#id_nombre').val('');

    $arbol.create_node(parent, newNode, position, false, false);
}

function arbol() {
    $.ajax({
        url: $url + 'arbol',
        success: function(r) {
            $('#arbol')
                .html('')
                .jstree('destroy')
                .on('changed.jstree', function(e, data) {
                    //$("#padre").val(data.instance.get_node(data.selected[0]).id);
                    //aplicacion.id = data.instance.get_node(data.selected[0]).id;
                })
                .jstree({
                    'core': {
                        //"animation" : 0,
                        "check_callback": true,
                        "themes": { "stripes": true },
                        'force_text': true,
                        "multiple": true,
                        'data': r
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder-o",
                            "valid_children": ["default", "file"]
                        },
                        "todo": {
                            "icon": "fa fa-sitemap",
                            "valid_children": []
                        },
                        "arch": {
                            "icon": "fa fa-file-text-o",
                            "valid_children": []
                        },
                        "metodo": {
                            "icon": "fa fa-gears",
                            "valid_children": []
                        }
                    },
                    "contextmenu": {
                        "items": function($node) {
                            return {
                                "Create": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "Crear",
                                    "action": function(obj) {
                                        $node = $arbol.create_node($node);
                                        $arbol.edit($node);
                                    }
                                },
                                "Rename": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "Renombrar",
                                    "action": function(obj) {
                                        $arbol.edit($node);
                                    }
                                },
                                "Remove": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "label": "Eliminar",
                                    "action": function(obj) {
                                        $arbol.delete_node($node);
                                    }
                                }
                            };
                        },
                        "select_node": true
                    },
                    "plugins": [
                        "search", "contextmenu", "types", "dnd"
                    ]
                });

            $arbol = $('#arbol').jstree(true);
        }
    });
}