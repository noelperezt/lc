var aplicacion, $form, tabla = false,
    to, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit': function(e) {
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes': function(accion) {
            if (accion !== 'guardar') return;

        },
        'limpiar': function() {
            $("select").val("").selectpicker("refresh");
        },
        'guardar': function(r) {
            aplicacion.limpiar();
        }
    });

    $form = aplicacion.form;

    $("#limpiar").click(function() {
        aplicacion.limpiar();
    });

    $("#id_pais").change(function() {
        reconstruirCombo("ciudad",{});
        aplicacion.selectCascada($(this).val(), "#id_ciudad", 'ciudades');
    });

    $("input[name=nombre_pais]").keypress(function(event) {
        if (event.which == 13)
            addPais();
    });

    $("input[name=nombre_ciudad]").keypress(function(event) {
        if (event.which == 13) {
            addCiudad();
        }
    });
});

function addPais() {
    if ($.trim($("input[name='nombre_pais']").val()) == '') {
        aviso("Ingrese un nombre de País válido", false);
        return false;
    }

    $existe = false;
    $("option", "#id_pais").each(function(){
        if($.trim($("input[name='nombre_pais']").val().toUpperCase()) == $.trim($(this).text().toUpperCase()))
            $existe = true;
    });
    
    if ($existe == true) {
        aviso("País ya existe en la lista");
        return false;
    };

    $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'pais/create/',
            data: {
                "csrfmiddlewaretoken": $("input[name='csrfmiddlewaretoken']").val(),
                "pais": $("input[name='nombre_pais']").val()
            },
            success: function (r) {
                aviso(r);

                if (r.s == 's') {
                    reconstruirCombo("pais", r.lista);
                }
            }
    });
}


function addCiudad() {
    if ($.trim($("input[name='nombre_ciudad']").val()) == '') {
        aviso("Ingrese un nombre de Ciudad válido", false);
        return false;
    }

    if ($.trim($("#id_pais").val()) == '') {
        aviso("Seleccione el Pais", false);
        return false;
    }

    $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'ciudad/create/',
            data: {
                "csrfmiddlewaretoken": $("input[name='csrfmiddlewaretoken']").val(),
                "pais": $("#id_pais").val(),
                "ciudad": $("input[name='nombre_ciudad']").val()
            },
            success: function (r) {
                aviso(r);

                if (r.s == 's') {
                    reconstruirCombo("ciudad", r.lista);
                }
            }
    });
}

function reconstruirCombo($nombre_combo, $opc) {
    $("select[name='" + $nombre_combo + "'] option").detach();

    $.each($opc, function(ll, v) {
        $("select[name='" + $nombre_combo + "']").append("<option value='" + ll + "'> " + v + "</option>");
    });

    $("select[name='" + $nombre_combo + "']").selectpicker('refresh');
}