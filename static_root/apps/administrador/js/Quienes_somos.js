var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
	aplicacion = new app("formulario", {
		'limpiar_inicio' : false,
		'limpiarDespuesGuardar' : false,
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;

			if (accion == 'guardar'){
				var $vacios = 0;
				$(".textareaTiny").each(function(){
					$(this).val(tinyMCE.get($(this).attr("id")).getContent());
					if($.trim($(this).val()) == '')				
						$vacios++;
				});

				if ($vacios >0){
					aviso("Complete los campos");
					return false;
				}
			}
		},
		'limpiar' : function(){
			///return false;
		},
		'buscar' : function(r){
			$("#foto").prop("src", r.archivo);		}
	});

	$("#id_resena, #id_mision,#id_vision").removeAttr('required');

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$(".btnCargaImg").click(function(){
		inputfile = $(this).attr("inputfile");
		$("#"+inputfile).trigger("click");
	});

	$("#fotousuario").change(function(){
		$("#foto").attr("src", $(this).val());
	});

	$form = aplicacion.form;
});