var aplicacion, $form, tabla = false,
    to, $arbol, imagenDefault = 'usuarios/None/no-img.jpg';

$(function() {
    aplicacion = new app("formulario", {
        'submit': function(e) {
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes': function(accion) {
            if (accion == 'guardar') {
                /*
                //Se comentó por solicitud que no sea obligatoria la imagen del usuario
                if($.trim($("#rutaimg").val()) == ''){
                	aviso("Debe ingresar la foto del usuario");
                	return false;
                }*/

                if ($("#id_groups").val() == 'null') {
                    aviso("Seleccione el perfil del usuario");
                    return false;
                }
            }

            //$("#permisos", $form).val(data_arbol());
        },
        'limpiar': function() {
            //$arbol.uncheck_all();
            tabla.fnDraw();

            $("#foto").prop("src", imagenDefault);
            $('#usuario').prop('readonly', false);
            $("#rutaimg").val("");
        },
        'buscar': function(r) {
            $("#id_is_superuser").prop("checked", r.is_superuser);
            $("#foto").prop("src", r.foto);
            $("#rutaimg").val(r.foto);
            var dateString = r.nacimiento;
            if (dateString == undefined) {
                dateString = ''
            }
            var string = dateString.replace(new RegExp('-', "g"), "/");
            var fechaN = string.split('/').reverse().join('/');
            $("#id_nacimiento").val(fechaN);

        }
    });

    $("#limpiar").click(function() {
        aplicacion.limpiar();
    });

    $form = aplicacion.form;

    $('input.campo').on("focus", function() {
        $(this).parents(".obj").addClass("campoActivo");
    }).on("blur", function() {
        $(this).parents(".obj").removeClass("campoActivo");
    }).on("click", "input.campo", function() {
        $(this).parents(".obj").removeClass("campoActivo");
    });

    $("#btnCargaImg").click(function() {
        $("#fotousuario").trigger("click");
    });

    $("#fotousuario").change(function() {
        $("#foto").attr("src", $(this).val());
        $("#rutaimg").val($(this).val());
    });

    $("#id_dni", $form).numeric({ min: 0 });
    $("#id_telefono", $form).mask("0999-999-9999");

    $('#id_nacimiento').datepicker({
        format: 'dd/mm/yyyy',
        language: 'es'
    });

    $('#input_buscar').keyup(function() {
        if (to) { clearTimeout(to); }
        to = setTimeout(function() {
            $arbol.search($('#input_buscar').val());
        }, 250);
    });


    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [{
            'targets': -1,
            'data': null,
            'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
        }]
    });

    $('.datatable').on('click', '.btnaccionedita', function(e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function(e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });

});

function data_arbol() {
    var $estructura = $arbol.get_json('#', { flat: true }),
        $seleccionados = $arbol.get_checked(),
        $data = [],
        direccion = '';

    $('.jstree-checkbox.jstree-undetermined', '#arbol').each(function() {
        var id = $(this).parent().parent().attr('id');

        if (id == '#') {
            return;
        }

        $seleccionados.push(id);
    });

    for (var i in $estructura) {
        if ($seleccionados.indexOf($estructura[i].id) === -1) {
            continue;
        }

        direccion = $estructura[i].li_attr['data-direccion'];

        if (direccion != '#' && $data.indexOf(direccion) === -1) {
            $data.push(direccion);
        }
    }

    return $data;
}

function arbol() {
    $.ajax({
        url: $url + 'arbol',
        success: function(r) {
            $('#arbol')
                .html('')
                .jstree('destroy')
                .on('changed.jstree', function(e, data) {
                    $("#padre").val(data.instance.get_node(data.selected[0]).id);
                    //aplicacion.id = data.instance.get_node(data.selected[0]).id;
                })

            .jstree({
                'core': {
                    //"animation" : 0,
                    "check_callback": true,
                    "themes": { "stripes": true },
                    'force_text': true,
                    "multiple": true,
                    'data': r
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder-o",
                        "valid_children": ["default", "file"]
                    },
                    "todo": {
                        "icon": "fa fa-sitemap",
                        "valid_children": []
                    },
                    "arch": {
                        "icon": "fa fa-file-text-o",
                        "valid_children": []
                    },
                    "metodo": {
                        "icon": "fa fa-gears",
                        "valid_children": []
                    }
                },
                "redraw_node": function(nodes) {
                    console.log(nodes);
                },
                "checkbox": {
                    //"keep_selected_style" : false
                },
                "plugins": [
                    "search", "checkbox", "types"
                ]
            });

            $arbol = $('#arbol').jstree(true);
        }
    });
}