var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;

			$("#id_descripcion").val(tinymce.get('id_descripcion').getContent());

			if($.trim($("#id_descripcion").val()) == ''){
				aviso("Debe completar la descripcion");
				return false;
			}

			if(!validarImagenCargada("input[name='archivo']")){
				aviso("Debe cargar una imagen");
				return false;	
			}
		},
		'limpiar' : function(){
			//$arbol.uncheck_all();
			tabla.fnDraw();
			$("#foto").prop("src", '');
			$('#usuario').prop('readonly', false);
		},
		'buscar' : function(r){
			$("#foto").prop("src", r.archivo);
			tinymce.get('id_descripcion').setContent(r.descripcion);
		}
	});

	$("#id_descripcion").removeAttr('required');

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$form = aplicacion.form;

	$(".btnCargaImg").click(function(){
		inputfile = $(this).attr("inputfile");
		$("#"+inputfile).trigger("click");
	});

	$("#fotousuario").change(function(){
		$("#foto").attr("src", $(this).val());
	});

	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});

	$('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });
});