var aplicacion, $form, tabla = false, to, imagenDefault = 'usuarios/None/no-img.jpg';

$(function() {
    /*  GENERAL PARA FORMULARIOS */
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion == 'guardar' || accion == 'actualizar'){
                if($.trim($("#rutaimg").val()) == ''){
                    aviso("Debe cargar imagen");
                    return false;
                }
            } 
        },
        'limpiar' : function(accion){
            tabla.fnDraw();
            
            $(".fotopreview").prop("src", imagenDefault);
            $("#id_archivo").val("");
            $("#rutaimg").val("");
        },
        'buscar' : function(r){
            $("#id_archivo").prop("src", r.archivo);
            $('.fotopreview').attr('src', r.archivo);
            $("#rutaimg").val(r.archivo);

        }
    });

    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <div class="btnaccion btnaccionedita"></div>\
                <div class="btnaccion btnaccioneliminar"></div>\
            '
        } ]
    });

    $('.datatable').on('click', '.btnaccionedita', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    /************      FIN      *****************/

    /************ JS TEMPLATE   *****************/

    $("#id_archivo").change(function(){
        previewImg(this, '.fotopreview');
        $("#rutaimg").val($(this).val());
    });
    
    $(".btnCargaImg").click(function(){
        $("#id_archivo").trigger("click");
    });

    $(".previsualizar").click(function(){
        if($.trim($("textarea[name='testimonial']").val()) == '' || $.trim($("#id_nombre").val()) == '' || $.trim($("#id_apellido").val()) == '' || $.trim($("#id_pais").val()) == ''){
            aviso("Debe completar los campos");
            return false;
        }
        $(".destestimonio", "#modalTestimonio").html('"' + $("textarea[name='testimonial']").val() + '"');
        $(".firmatestimonio", "#modalTestimonio").html($("#id_nombre").val() + " " + $("#id_apellido").val() + "<br>" + $("option[value='"+$("#id_pais").val()+"']", "#id_pais").html());

        $('#modalTestimonio').modal('show');
    });

    $("#btnAprobar").click(function(){
        aviso("Testimoniales Aprobados");   
    });

    aplicacion.limpiar();
});