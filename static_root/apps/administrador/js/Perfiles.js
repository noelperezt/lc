var aplicacion, arbol_ele, to = false,
    tabla = false,
    $arbol;
$(function() {
    aplicacion = new app("formulario", {
        'antes': function(accion) {
            if (accion == 'guardar') {
                if ($.trim($("#id_name").val()) == '') {
                    aviso("Debe ingresar un nombre al perfil");
                    return false;
                }
            }
            if (accion !== 'guardar') return;

            var data = data_arbol();
            $("#permisos").val(data.join(','));
        },
        'limpiar': function() {
            $arbol.uncheck_all();
            tabla.fnDraw();
        },
        'buscar': function(r) {
            $arbol.uncheck_all();

            for (var i in r.permisos) {
                $arbol.check_node(r.permisos[i]);
            }
            /*
            var $estructura = $arbol.get_json('#', { flat: true });
            r.permisos;
            for (var i in $estructura) {
                if ($estructura[i].icon == "fa fa-folder-o" || 
                    $estructura[i].li_attr['data-direccion'].substring(0, 1) == '#' || 
                    r.permisos.indexOf($estructura[i].li_attr['data-direccion']) === -1) {
                    continue;
                }

                $arbol.check_node($estructura[i].id);
            }
            */
        }
    });

    arbol();

    $(document).on("focus", "input.campo", function() {
        $(this).parents(".obj").addClass("campoActivo");
    });

    $(document).on("blur", "input.campo", function() {
        $(this).parents(".obj").removeClass("campoActivo");
    });

    $('#input_buscar').keyup(function() {
        if (to) { clearTimeout(to); }
        to = setTimeout(function() {
            $arbol.search($('#input_buscar').val());
        }, 250);
    });

    tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});
    $('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

    //$("#guardar").click(function(){
    //    aplicacion.guardar();
    //});

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });
});

function data_arbol() {
    return $arbol.get_checked();
}

function arbol() {
    $.ajax({
        url: $url + 'arbol',
        success: function(r) {
            $('#arbol')
                .html('')
                .jstree('destroy')
                .on('changed.jstree', function(e, data) {
                    //$("#padre").val(data.instance.get_node(data.selected[0]).id);
                    //aplicacion.id = data.instance.get_node(data.selected[0]).id;
                })
                .jstree({
                    'core': {
                        //"animation" : 0,
                        "check_callback": true,
                        "themes": { "stripes": true },
                        'force_text': true,
                        "multiple": true,
                        'data': r
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder-o",
                            "valid_children": ["default", "file"]
                        },
                        "todo": {
                            "icon": "fa fa-sitemap",
                            "valid_children": []
                        },
                        "arch": {
                            "icon": "fa fa-file-text-o",
                            "valid_children": []
                        },
                        "metodo": {
                            "icon": "fa fa-gears",
                            "valid_children": []
                        }
                    },
                    "checkbox": {
                        //"keep_selected_style" : false
                    },
                    "plugins": [
                        "search", "checkbox", "types"
                    ]
                });

            $arbol = $('#arbol').jstree(true);
        }
    });
}