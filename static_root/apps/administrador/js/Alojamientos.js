var aplicacion, $form, tabla = false, to, $arbol, clonPlan = 0, clonMobiliario = 0;

$(function() {
    aplicacion = new app("formulario",{
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

            return true;
        },
        'limpiar' : function(){
            tabla.fnDraw();
            $(".previewimg").prop("src", '');
            $(".contenedorMultimedia").html("");
        },
        'buscar' : function(r){
            $('.toggles').toggles(r.disponible);

            $("#foto").prop("src", r.archivo);
            tinymce.get('id_descripcion').setContent(r.descripcion);
        }
    });

    $form = aplicacion.form;

    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <div class="btnaccion btnaccionedita"></div>\
                <div class="btnaccion btnaccioneliminar"></div>\
            '
        } ]
    });

    $('.datatable').on('click', '.btnaccionedita', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('#tiposLc').on('click', '.fa-save', function (e) {
        var nombre = $("#nombre_tipo_servicio").val();
        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_tipo/',
            data: {
                'nombre': nombre,
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (data) {
                $(".inputguardar").val('');
                
            }
        });
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $(".inputfile").change(function(){
        previewImg(this, $(this).attr("destinopreview"));
    });
    
    $(".btnCargaImg").click(function(){
        inputfile = $(this).attr("inputfile");
        $("input[name='" + inputfile + "']").trigger("click");
    });

    $("#id_pais").change(function(){
        aplicacion.selectCascada($(this).val(), "#id_ciudad", 'ciudades', "refreshSelectpickers");
    });

    $("#btn_add_plan").click(function(){
        $("#modalPlan").modal("show");
    });

    $("#btnGuardarplan").click(function(){
        if($.trim($("#nombre_plan", "#modalPlan").val()) == '' || $.trim($("#precio_plan", "#modalPlan").val()) == '')
            return false;

        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_plan/',
            data: {
                'nombre_plan': $("#nombre_plan", "#modalPlan").val(),
                'precio_plan': $("#precio_plan", "#modalPlan").val(),
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (r) {
                aviso(r.msj);

                if(r.s == 's'){
                    addTrPlan($nombre, $precio);
                    $("#modalPlan").modal("hide");
                }                
            }
        });
    });

    $("#toggleDisponible").toggles({
        text: {on: 'SI', off: 'NO'},
        on: true,
        width: 45,
        height: 20
    }).on('toggle', function(e, active) {
        $("#id_disponible").val(active);
    });

    $("#btnAddMobiliario").click(function(){
        addTrMobiliario("", 1);
    });

    $("#btnCargaUsuario").click(function(){
        $("#modalUsuario").modal("show");
    });

    $("#btnCargaImg").click(function(){
        $("#fotousuario").trigger("click");
    });

    $("#fAddUsuario").submit(function(e){
        e.preventDefault();

        if($("input[type=text]:empty, input[type=password]:empty", "#fAddUsuario").length > 0){
            aviso("Debe completar los campos", false);
            return false;
        }

        if(!validarEmail($("input[name=email]", "#fAddUsuario").val())){
            aviso("Correo Inválido", false);
            return false;   
        }

        if($.trim($("#fotousuario").val()) == ''){
            aviso("Debe ingresar la foto del usuario");
            return false;
        }

        var parametros=new FormData($(this)[0]);

        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_usuario/',
            data: parametros,
            contentType: false,
            processData: false,
            success: function (r) {
                aviso(r.msj);

                if(r.s == 's'){
                    $("#modalUsuario").modal("hide");
                    // debo colocar el id del usuario, el nombre y la foto en los respectivos campos de la seccion arrendatario
                }                
            }
        });
    });

    $("#btnGuardarUsuario").click(function(){
        $("#fAddUsuario").submit();
    });
});     

function addTrPlan($id_plan, $nombre_plan, $precio_plan){
    clonPlan++;
    $(".trPlan[clon=0]").clone().attr("clon", clonPlan).appendTo("#tbl_planes");

    $("input[name='id_plan[]']", ".trPlan[clon="+ clonPlan +"]").val($id_plan);
    $(".td_nombre_plan", ".trPlan[clon="+ clonPlan +"]").html($nombre_plan);
    $(".td_precio_plan", ".trPlan[clon="+ clonPlan +"]").html($precio_plan);
}

function addTrMobiliario($nom_mobiliario, $cant_mobiliario){
    clonMobiliario++;
    $(".trMobiliario[clon=0]").clone().attr("clon", clonMobiliario).appendTo("#tbl_mobiliario");

    $("input[name='nom_mobiliario[]']", ".trMobiliario[clon="+ clonMobiliario +"]").html($nom_mobiliario);
    $("input[name='cant_mobiliario[]']", ".trMobiliario[clon="+ clonMobiliario +"]").html($cant_mobiliario);
}

function borraMobiliario($btn){
    $tr = $($btn).parents(".trMobiliario");

    $("input", $tr).val("");

    if($tr.attr("clon") != '0')
    $tr.detach();
}