var aplicacion, $form, tabla = false, to;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;

			$(".textareaTiny").each(function(){
				$(this).val(tinyMCE.get($(this).attr("id")).getContent());				
			});
		},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar'	: function(r){
			if (r.s == 's') {
				$('#myModal').modal('show');
			}
		}
	});

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$form = aplicacion.form;

	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});

	$('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });
});