var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

            if($(".ItemMultimedia[clon!='0']").length == 0){
                aviso("Debe cargar un archivo");
                return false;   
            }
        },
        'limpiar' : function(){
            tabla.fnDraw();
            $(".ItemBannerMultimedia[clon!='0']").detach();
        },
        'buscar' : function(r){
            //addBannerBqda($contenedorDestino, $id_multimedia, $rutaFull, $alt, $nombreimg, $activo, $posicion);
        }
    });

    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <div class="btnaccion btnaccionedita"></div>\
                <div class="btnaccion btnaccioneliminar"></div>\
            '
        } ]
    });

    $('.datatable').on('click', '.btnaccionedita', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $("#btnCargaImg").click(function(){
        $("#archivo").trigger("click");
    });

    $(".previsualizar").click(function(){
        $(".img_ver_banner", "#modalBanner").attr("src", $("#archivo").val());
        $('#modalBanner').modal('show');
    });    
});

function addBannerMultimedia($contenedorDestino){
    $(".imgseleccionada img").each(function(){
        $clonMultimedia++;

        $(".ItemBannerMultimedia[clon='0']").clone().attr("clon", $clonMultimedia).css("display", "block").appendTo($contenedorDestino);
        $padre = $(".ItemBannerMultimedia[clon='"+ $clonMultimedia +"']");

        $("img", $padre).attr({"src" : $(this).attr("src"), "alt" : $(this).attr("alt")});
        $("input[name='id_multimedia[]']", $padre).val($(this).attr("id_multimedia"));
        $("input[name='posicion[]']", $padre).val(1);
        $("input[name='activo[]']", $padre).val(true);
        $(".nombreImg", $padre).html($(this).attr("nombreimg"));
        
        $('.toggle').toggles({
            text: {on: 'SI', off: 'NO'},
            on: true,
            width: 45,
            height: 20
        }).on('toggle', function(e, active) {
            $p = $(this).parents(".ItemBannerMultimedia");
            $("input[name='activo[]']", $p).val(active);
        });
    });

}

function addBannerBqda($contenedorDestino, $id_multimedia, $rutaFull, $alt, $nombreimg, $activo, $posicion){
    if ($activo == 'undefined')
        $activo = true;

    $clonMultimedia++;

    $(".ItemBannerMultimedia[clon='0']").clone().attr("clon", $clonMultimedia).css("display", "block").appendTo($contenedorDestino);
    
    $padre = $(".ItemBannerMultimedia[clon='"+ $clonMultimedia +"']");

    $("img", $padre).attr({"src" : $rutaFull, "alt" : $alt});
    $("input[name='id_multimedia[]']", $padre).val($id_multimedia);
    $("input[name='posicion[]']", $padre).val($posicion);
    $("input[name='activo[]']", $padre).val($activo);
    $(".nombreImg", $padre).html($nombreimg);

    $('.toggle', $padre).toggles({
        text: {on: 'SI', off: 'NO'},
        on: $activo,
        width: 45,
        height: 20
    }).on('toggle', function(e, active){
        $p = $(this).parents(".ItemBannerMultimedia");
        $("input[name='activo[]']", $p).val(active);
    });    
}