var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;

			//$("#permisos", $form).val(data_arbol());
		},
		'limpiar' : function(){
			//$arbol.uncheck_all();
			tabla.fnDraw();
			$("#foto").prop("src", imagenDefault);
			$('#usuario').prop('readonly', false);
		},
		'buscar' : function(r){

			console.log(r.foto);
			$("#foto").prop("src", r.foto);

		}
	});

	$("#limpiar").click(function(){
		aplicacion.limpiar();
	});

	$form = aplicacion.form;

	$(".btnCargaImg").click(function(){
		inputfile = $(this).attr("inputfile");
		$("#"+inputfile).trigger("click");
	});

	$("#fotousuario").change(function(){
		$("#foto").attr("src", $(this).val());
	});

	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<button class="btn btn-primary">Editar</button>\
				<button class="btn btn-danger">Eliminar</button>\
			'
		} ]
	});

	$('.datatable').on('click', '.btn-primary', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btn-danger', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	tinymce.init({
      selector: '.textareaTiny',
      height: 300,
      theme: 'modern',
      plugins: [
        'advlist autolink lists link image charmap preview hr anchor pagebreak',
        'searchreplace wordcount',
        'insertdatetime media nonbreaking save table contextmenu ',
        'emoticons  paste textcolor colorpicker textpattern'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      toolbar2: 'forecolor backcolor emoticons',
      image_advtab: true
     });
});