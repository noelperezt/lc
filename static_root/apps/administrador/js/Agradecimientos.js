var aplicacion, $form, tabla = false, to, $arbol;

$(function() {
	aplicacion = new app("formulario", {
		'submit' : function(e){
			e.preventDefault();
			aplicacion.guardar();
		},
		'antes' : function(accion){
			if (accion !== 'guardar') return;

			if(!validarImagenCargada("input[name='archivo']")){
				aviso("Debe cargar una imagen");
				return false;	
			}
		},
		'limpiar' : function(){
			tabla.fnDraw();
		},
		'buscar' : function(r){

		}
	});

	$(".btnCargaImg").click(function(){
		inputfile = $(this).attr("inputfile");
		$("#"+inputfile).trigger("click");
	});

	$("#fotousuario").change(function(){
		$("#foto").attr("src", $(this).val());
	});
	

	$form = aplicacion.form;
	tabla = datatableview.initialize($('.datatable'), {
		'columnDefs': [ {
			'targets': -1,
			'data': null,
			'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
		} ]
	});

	$('.datatable').on('click', '.btnaccionedita', function (e) {
		var id = $(this).parents('tr').attr('id');
		aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function (e) {
        var id = $(this).parents('tr').attr('id');
		aplicacion.eliminar(id);
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $(".textareaTiny").removeAttr("required");
});