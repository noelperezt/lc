var $clonMultimedia = 0, $botonInicialSeleccionado, $archivosUpload, $clonNuevoMultimedia = 0;

$(document).ready(function () {
    'use strict';

    $("#form_fileupload", "#modalMultimedia").attr("action", $url + "multimedia");

    $(".fileinput-button", "#modalMultimedia").click(function(){
        $('#fileupload', "#modalMultimedia").trigger("click");
    });

    $('#fileupload').fileupload({
        url: '/administrador/multimedia/',
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|mp4|webm)$/i,
        maxFileSize: 999000,
        add     : function(e, data) {
            $clonNuevoMultimedia;

            $.each(data.files, function(index, file) {
                $clonNuevoMultimedia++;

                data.context = $('.nuevaImagen[clon=0]').clone().attr("clon", $clonNuevoMultimedia).show("fast").appendTo('#files');

                previewImgMultimedia('.nuevaImagen[clon="' + $clonNuevoMultimedia + '"] .imgPreviewMultimedia', file);

                $("input[name='nom_multimedia_cliente']", '.nuevaImagen[clon="' + $clonNuevoMultimedia + '"]').val(file.name);

                $(".fa-upload", '.nuevaImagen[clon="' + $clonNuevoMultimedia + '"]' ).data(data);

                /*$(".fa-upload", '.infoNuevaImg').on("click", function(){
                    data = $(this).data();

                    $padre = $(this).parents('.nuevaImagen');

                    if($.trim($("input[name='titulo_subir_multimedia']", $padre).val()) == '' || $("input[name='alt__subir_multimedia']", $padre).val() == ''){
                        aviso("Ingrese los datos solicitados");
                        return false;
                    }

                    data.formData = {
                        'nom_multimedia_cliente'    :   $("input[name='nom_multimedia_cliente']", $padre).val(),
                        'titulo_subir_multimedia'   :   $("input[name='titulo_subir_multimedia']", $padre).val(),
                        'alt__subir_multimedia'     :   $("input[name='alt__subir_multimedia']", $padre).val()
                    };

                    data.submit().always(function() {
                        $(this).parents(".nuevaImagen").remove();
                    });
                });*/
                
                $(".fa-trash", '.nuevaImagen').on("click", function(){
                    $(this).parents(".nuevaImagen").remove();
                    refreshCargadorMultimedia();
                });
            });

            refreshCargadorMultimedia();
        },
        'success'   : function(r, textStatus, jqXHR){
            if(r.s == 's'){
                $(".datosCompletosMultimedia").remove();

                var $a = { 0 : {"nombre_archivo" : r.full, "id_multimedia": r.id, "nombre" : r.nombre, "titulo" : r.titulo, "texto_alternativo" : r.alt}};

                colocaEnBiblioteca($a, "antes");                
            }
        }
    })

    $("#btnAddMultimedia").click(function(){    
        if($botonInicialSeleccionado.attr("funcion") != undefined){
            var $f = $botonInicialSeleccionado.attr("funcion")

            eval($f + "('" + $botonInicialSeleccionado.attr("contenedor") + "')");
        }else{
            agregarMultimediaSeleccionada($botonInicialSeleccionado.attr("contenedor"));
        }
      
        $("#modalMultimedia").modal("hide");
    });

    $("#btn_guardar_multimedia").click(function(){
        var $v = 0;

        $("input[type=text]", ".infoimg").removeClass("falta_requerido");

        $("input[type=text]", ".infoimg").each(function(){
            if ($.trim($(this).val()) == ''){
                $(this).addClass("falta_requerido");
                $v++;
            }
        });

        if($v > 0){
            aviso("Debe completar los campos", false);
            return false;
        }

        $.ajax({
            'type' : 'POST',
            url: '/administrador/multimedia/info/',
            data: $("#fInfoImg").serialize(),
            success: function(r) {
                aviso(r.msj);
                if(r.s == 's'){
                    $(".imgBiblioteca[id_multimedia='" + r.id + "']", "contenedorBiblioteca").attr({title : r.titulo, alt : r.alt});
                }
            }
        });
    });

    $("#btn_eliminar_multimedia").click(function(){
        (new PNotify({
            title: 'Confirmar',
            text: 'Realmente desea eliminar este archivo?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        })).get().on('pnotify.confirm', function() {
            $.ajax({
                url: '/administrador/multimedia/info/',
                data: $("#fInfoImg").serialize(),
                success: function(r) {
                    aviso(r.msj);
                    if(r.s == 's')
                        refrescarMultimedia();                    
                }
            });
        });
    });

    $("#btnFiltrarMultimedia").click(function(){
        refrescarMultimedia();
    });

    $(".btnSelMultimedia").click(function(){
        $botonInicialSeleccionado = $(this);

        ocultaImgsUsadas($botonInicialSeleccionado.attr("contenedor"));

        $("#modalMultimedia").modal("show");
    });

    $("#btnCargarMasMultimedia").click(function(){
        $(this).hide();
        $("#loaderBiblioteca").show();

        $.ajax({
            url     : '',
            data    : {
                pag     :   2
            },
            success : function(){
                $("#loaderBiblioteca").hide();    
                
                if (r.hay_mas    == 's') {
                    $(this).show();
                }
            }
        });
    });

    $("#subir_todos", "#modalMultimedia").on("click", function(){
        $(".nuevaImagen", "#files").removeClass("datosIncompletosMultimedia");

        $(".fa-upload", "#files").each(function(){
            $(this).trigger("click");            
        });
    });
});

function activaImg($img){
    $($img).toggleClass("imgseleccionada");

    if($($img).hasClass("imgseleccionada")){
        $("#id_multimedia").val($("img", $img).attr("id_multimedia"));
        $("#id_nombre_imagen_multimedia").val($("img", $img).attr("nombreimg"));
        $("#id_titulo_imagen_multimedia").val($("img", $img).attr("title"));
        $("#id_alt_imagen_multimedia").val($("img", $img).attr("alt"));
    }

    if($(".imgseleccionada", ".contenedorBiblioteca").length >= 1)
        $("#btnAddMultimedia", "#modalMultimedia").show();
    else
        $("#btnAddMultimedia", "#modalMultimedia").hide();

    if($(".imgseleccionada", ".contenedorBiblioteca").length != 1){
        $(".infoimg").hide();
    }else{
        $(".infoimg").show();
    }
}

function agregarMultimediaSeleccionada($contenedorDestino){
    $(".imgseleccionada img").each(function(){
        $clonMultimedia++;

        $(".ItemMultimedia[clon='0']").clone().attr("clon", $clonMultimedia).css("display", "block").appendTo($contenedorDestino);
        $padre = $(".ItemMultimedia[clon='"+ $clonMultimedia +"']");

        $("img", $padre).attr({"src" : $(this).attr("src"), "alt" : $(this).attr("alt")});
        $("input", $padre).val($(this).attr("id_multimedia"));
        $("label", $padre).html($(this).attr("nombreimg"));
    });

    $(".contenedorMultimedia").css({width : ($(".ItemMultimedia[clon!='0']").length * 172) + "px"});
}

function removerMultimediaDelContenedor($span){
    $($span).parents(".ItemMultimedia").detach();
}

function refrescarMultimedia(){
    $(".archivoBiblioteca", ".contenedorBiblioteca").detach();

    $.ajax({
        url: '/administrador/multimedia/refrescar/',
        data: $("#fFiltrosMultimedia").serialize(),
        success: function(r) {
            if (r.s == 's') {
                colocaEnBiblioteca(r.multimedia, "despues");
            }                
        }
    });
}

function colocaEnBiblioteca(multimedia, $posicion){
    $.each(multimedia, function(ll, v) {
        extension = getFileExtension2(v.nombre_archivo); 

        if (extension == 'mp4' || extension == 'webm'){
            v.nombre_archivo = '/static/img/video.png';
        }else if(extension == 'pdf'){
            v.nombre_archivo = '/static/img/pdf.png';
        }

        $str = "<div class='archivoBiblioteca' onClick='activaImg(this);'><img class='imgBiblioteca' src='" + v.nombre_archivo + "' id_multimedia='" + v.id_multimedia + "' nombreimg='" + v.nombre + "' title='" + v.titulo + "' alt='" + v.texto_alternativo + "'/> <label class='nombreMultimedia'> " + v.nombre + " </label></div>";

        if($posicion == 'antes')
            $(".contenedorBiblioteca").prepend($str);
        else
            $(".contenedorBiblioteca").append($str);            
    });
}

function agregarMultimediadeRegistro($json){
    $.each($json, function(ll, v){
        $clonMultimedia++;

        $(".ItemMultimedia[clon='0']").clone().attr("clon", $clonMultimedia).css("display", "inline-block").appendTo($contenedorDestino);
        $padre = $(".ItemMultimedia[clon='"+ $clonMultimedia +"']");

        $("img", $padre).attr({"src" : v.nombre_archivo, "alt" : v.texto_alternativo});
        $("input", $padre).val(v.id_multimedia);
    });
}

function previewImgMultimedia(selectorDestino, $ruta_preview){
    var reader = new FileReader();

    extension = getFileExtension2($ruta_preview.name); 

    if (extension == 'mp4' || extension == 'webm'){
        $(selectorDestino).attr('src', '/static/img/video.png');
        return false;
    }else if(extension == 'pdf'){
        $(selectorDestino).attr('src', '/static/img/pdf.png'); 
        return false;
    }

    reader.onload = function (e) {
        $(selectorDestino).attr('src', e.target.result);
    }
    reader.readAsDataURL($ruta_preview);
}

function getFileExtension2(filename) {
  return filename.split('.').pop();
}

function ocultaImgsUsadas($contenedor){
    $(".archivoBiblioteca").css("display", "block");
    $(".infoimg").css("display", "none");

    $(".ItemMultimedia input[name='id_multimedia[]']", $contenedor).each(function(){
        $(".archivoBiblioteca .imgBiblioteca[id_multimedia='" + parseInt($(this).val()) + "']").parents(".archivoBiblioteca").removeClass("imgseleccionada").css("display", "none");
    });

    if($("imgseleccionada").length > 0)
        $(".infoimg").css("display", "show");
}

function refreshCargadorMultimedia(){
    if($(".nuevaImagen", "#files").length > 0)
        $(".cargador_multimedia", "#modalMultimedia").css("float", "left");
    else
        $(".cargador_multimedia", "#modalMultimedia").css("float", "none");
}

function subirArchivoMultimedia($boton){
        data = $($boton).data();

        var $padre = $($boton).parents('.nuevaImagen');

        if($.trim($("input[name='titulo_subir_multimedia']", $padre).val()) == '' || $("input[name='alt__subir_multimedia']", $padre).val() == ''){
            $padre.addClass("datosIncompletosMultimedia");
            aviso("Ingrese los datos solicitados");
            return false;
        }else{
            $padre.addClass("datosCompletosMultimedia");            
        }

        data.formData = {
            'nom_multimedia_cliente'    :   $("input[name='nom_multimedia_cliente']", $padre).val(),
            'titulo_subir_multimedia'   :   $("input[name='titulo_subir_multimedia']", $padre).val(),
            'alt__subir_multimedia'     :   $("input[name='alt__subir_multimedia']", $padre).val()
        };

        data.submit();
}