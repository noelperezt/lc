var aplicacion, $form, tabla = false,
    to, $arbol;

$(function() {
    aplicacion = new app("formulario", {
        'submit': function(e) {
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes': function(accion) {
            if (accion !== 'guardar') return;

            if (accion == 'guardar') {
                var $vacios = 0;
                $(".textareaTiny").each(function() {
                    $(this).val(tinyMCE.get($(this).attr("id")).getContent());
                    if ($.trim($(this).val()) == '')
                        $vacios++;
                });

                if ($vacios > 0) {
                    aviso("Complete los campos");
                    return false;
                }
            }
        },
        'limpiar': function() {
            tinyMCE.activeEditor.setContent("");
            $(".contenedorMultimedia").remove();
            tabla.fnDraw();
        },
        'buscar': function(r) {
            tinymce.get('id_descripcion').setContent(r.descripcion);
        }
    });

    $("#id_descripcion").removeAttr('required')

    $form = aplicacion.form;

    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [{
            'targets': -1,
            'data': null,
            'defaultContent': '\
				<div class="btnaccion btnaccionedita"></div>\
				<div class="btnaccion btnaccioneliminar"></div>\
			'
        }]
    });

    $('.datatable').on('click', '.btnaccionedita', function(e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.btnaccioneliminar', function(e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });

    $('#tabla').on("click", "tbody tr", function() {
        aplicacion.buscar(this.id);
    });
});