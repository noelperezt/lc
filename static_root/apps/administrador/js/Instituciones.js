var aplicacion, $form, tabla = false, to, $arbol, clonPlan = 0;

$(function() {
    aplicacion = new app("formulario", {
        'submit' : function(e){
            e.preventDefault();
            aplicacion.guardar();
        },
        'antes' : function(accion){
            if (accion !== 'guardar') return;

            $("#id_descripcion").val(tinymce.get('id_descripcion').getContent());
            return true;
        },
        'limpiar' : function(){
            tabla.fnDraw();
            $(".previewimg").prop("src", '');
            $(".contenedorMultimedia").html("");
        },
        'buscar' : function(r){
            $("#foto").prop("src", r.archivo);
            tinymce.get('id_descripcion').setContent(r.descripcion);
        }
    });    

    $form = aplicacion.form;
    tabla = datatableview.initialize($('.datatable'), {
        'columnDefs': [ {
            'targets': -1,
            'data': null,
            'defaultContent': '\
                <i class="fa fa-pencil" aria-hidden="true"></i>\
                <i class="fa fa-trash" aria-hidden="true"></i>\
            '
        } ]
    });

    $("#limpiar").click(function(){
        aplicacion.limpiar();
    });

    $('.datatable').on('click', '.fa-pencil', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.buscar(id);
    });

    $('.datatable').on('click', '.fa-trash', function (e) {
        var id = $(this).parents('tr').attr('id');
        aplicacion.eliminar(id);
    });
    $('#tiposLc').on('click', '.fa-save', function (e) {
        var nombre = $("#nombre_tipo_servicio").val();
        $.ajax({
            dataType: 'json',
            method: 'POST',
            url: $url + 'add_tipo/',
            data: {
                'nombre': nombre,
                'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function (data) {
                $(".inputguardar").val('');
                
            }
        });
    });

    $('#tabla').on("click", "tbody tr", function(){
        aplicacion.buscar(this.id);
    });

    $(".inputfile").change(function(){
        previewImg(this, $(this).attr("destinopreview"));
    });

    $(".btnCargaImg").click(function(){
        inputfile = $(this).attr("inputfile");
        $("input[name='" + inputfile + "']").trigger("click");
    });

    /*  WIZARD  */

    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
       //alert("You are on step "+stepNumber+" now");
       if(stepPosition === 'first'){
           $("#prev-btn").addClass('disabled');
       }else if(stepPosition === 'final'){
           $("#next-btn").addClass('disabled');
       }else{
           $("#prev-btn").removeClass('disabled');
           $("#next-btn").removeClass('disabled');
       }
    });

    var btnFinish = $('<button></button>').text('Finalizar')
                                             .addClass('btn btn-info')
                                             .on('click', function(){ alert('Finalizado'); });
    var btnCancel = $('<button></button>').text('Cancelar')
                                     .addClass('btn btn-danger')
                                     .on('click', function(){ $('#smartwizard').smartWizard("reset"); });
    
    // Smart Wizard
    $('#smartwizard').smartWizard({ 
            selected: 0, 
            theme: 'circles',
            transitionEffect:'fade',
            showStepURLhash: false,
            toolbarSettings: {
                toolbarPosition: 'both',
                toolbarExtraButtons: [btnFinish, btnCancel]
            },
            lang: {  // Language variables
                next: 'Siguiente', 
                previous: 'Anterior'
            },
    });

    $("#catalogo_institucion").change(function(){
        $("#nombrecatalogo").html(this.files[0].name);
    });

    $("#id_video_institucion").change(function(){
        $("#nombrevideo").html(this.files[0].name);
    });

    $("input[name='alumnos_aulas'], input[name='capacidad_aulas']").change(function(){
        $("input[name='capacidad_alumnos']").val(parseInt($("input[name='alumnos_aulas']").val()) * parseInt($("input[name='capacidad_aulas']").val()));
    });
});