from django.apps import AppConfig


class AlojamientoConfig(AppConfig):
    name = 'apps.alojamiento'
