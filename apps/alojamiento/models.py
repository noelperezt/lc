from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _
from apps.administrador.models import Servicios, Multimedia, Usuarios


class Recargos(models.Model):

    monto      = models.DecimalField(max_digits=10, decimal_places=2, null=False, blank=False, verbose_name=_('Monto'))
    mes        = JSONField(null=False, blank=False, verbose_name=_('Mes'))
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table            = 'recargos'
        verbose_name        = _('Recargo')
        verbose_name_plural = _('Recargos')

        permissions = (
            ("view_recargos", "Can view Recargo"),
            ("detail_recargos", "Can detail Recargo"),
        )

class Inmuebles_tipo(models.Model):
    descripcion = models.CharField(max_length=60, null=True, blank=True)
    activo = models.BooleanField(null=False, blank=False, default=True, verbose_name=_('Activo'))

    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))
    

class Inmuebles(models.Model):    
    descripcion = models.TextField()
    normas = models.TextField(null=True, blank=True)
    precio_lc = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    precio_sede = models.DecimalField(max_digits=10, decimal_places=2, null=False, blank=False)
    precio_semanal = models.BooleanField(default=True, blank=False, null=False)
    activo = models.BooleanField(default=False)
    servicios = models.ManyToManyField(Servicios)
    arrendatario = models.ForeignKey(Usuarios)
    archivo = models.ManyToManyField(Multimedia)
    recargo = models.ForeignKey(Recargos, on_delete=models.PROTECT,null=True, blank=True)
    tipo_inmuebles = models.ForeignKey(Inmuebles_tipo, on_delete=models.PROTECT,null=True, blank=True)

    created_at     = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at      = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table            = 'inmuebles'
        verbose_name        = _('Inmueble')
        verbose_name_plural = _('Inmuebles')

        permissions = (
            ("view_inmuebles", "Can view Inmueble"),
            ("detail_inmuebles", "Can detail Inmueble"),
        )

class Mobiliarios(models.Model):

    descripcion = models.TextField(max_length=80, verbose_name=_('Descripción'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table            = 'mobiliarios'
        verbose_name        = _('Mobiliario')
        verbose_name_plural = _('Mobiliarios')

        permissions = (
            ("view_mobiliarios", "Can view Mobiliario"),
            ("detail_mobiliarios", "Can detail Mobiliario"),
        )


class Inmuebles_mobiliario(models.Model):

    cantidad   = models.IntegerField(default=0, verbose_name=_('Cantidad'))
    mobiliario = models.ForeignKey(Mobiliarios, verbose_name=_('Mobiliario'))
    inmueble   = models.ForeignKey(Inmuebles, verbose_name=_('Inmueble'))
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table            = 'inmuebles_mobiliario'
        verbose_name        = _('Inmueble Mobiliario')
        verbose_name_plural = _('Inmuebles Mobiliario')

        permissions = (
            ("view_inmuebles_mobiliario", "Can view Inmueble Mobiliario"),
            ("detail_inmuebles_mobiliario", "Can detail Inmueble Mobiliario"),
        )


