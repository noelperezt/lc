from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group

from ..models import Usuarios


class UsuariosForm(forms.ModelForm):

    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all(), 
        label='Perfiles',
        widget=forms.SelectMultiple(attrs={ 
            'class' : 'selectpicker show-menu-arrow form-control required' 
        })
    )
    password = forms.CharField(label=_("Password"), required=False, widget=forms.PasswordInput)
    nacimiento = forms.DateInput()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'groups':
                continue
            if campo != 'nacimiento':
                self.fields[campo].widget.attrs.update({
                    'class' : 'form-control campo',
                    'placeholder' : self.fields[campo].label
                })
            else:
                self.fields[campo].widget.attrs.update({
                    'class': 'form-control campo',
                    'placeholder': 'dd/mm/yyyy'
                })


        self.fields['pais'].widget.attrs.update({'class' : 'form-control campo selectpicker', 'data-live-search':'true'})
        self.fields['genero'].widget.attrs.update({'class' : 'form-control campo selectpicker'})
        self.fields['nacimiento'].widget.attrs.update({'readonly' : 'readonly'})


    def save(self, commit=True):
        instance = super(UsuariosForm, self).save(commit=False)
        password = self.cleaned_data["password"]

        self.cleaned_data['email'] = self.cleaned_data['email'].lower()
        
        if instance.pk:
             if password != '':
                 instance.set_password(password)
        else:
             instance.set_password(password)

        old_save_m2m = self.save_m2m
        def save_m2m():
            old_save_m2m()
           
            instance.groups.clear() 
            for perfil in self.cleaned_data['groups']:
                instance.groups.add(perfil)

        self.save_m2m = save_m2m

        if commit:
            instance.save()
            self.save_m2m()

        return instance


    class Meta:
        model = Usuarios
        fields = [
            'nombre', 
            'apellido', 
            'email', 
            'password', 
            'dni', 
            'genero', 
            'nacimiento', 
            'direccion', 
            'telefono', 
            'foto', 
            'pais', 
            'groups'##,
           ## 'is_superuser'
        ]
        #localized_fields = ('nacimiento',)
        widgets = {
            'direccion': forms.Textarea(attrs={'class': 'form-control campo', 'rows': 1, 'cols': 200, 'style': 'resize:none;'})
        }