from django import forms
from django.utils.translation import ugettext_lazy as _

from ..models import Oficinas, Paises, Ciudades


class OficinasForm(forms.ModelForm):

    pais = forms.ModelChoiceField(queryset=Paises.objects.all(), empty_label=_('- Seleccione un Pais'))
    ciudad = forms.ModelChoiceField(queryset=Ciudades.objects.none(), empty_label=_('- Seleccione una Ciudad'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['pais'].widget.attrs.update({'class' : 'form-control campo selectpicker','data-live-search':'true','data-width':'100%'});
        #self.fields['ciudad'].widget.attrs.update({'class' : 'form-control campo selectpicker'})

    class Meta:
        model = Oficinas
        fields = ['email', 'pais', 'ciudad', 'direccion', 'iframe_ubicacion']
        widgets = {
            'direccion': forms.Textarea(attrs={'class': 'form-control', 'required':'required', 'rows': 5, 'cols': 200, 'style': 'resize:none;'})
        }
