from django import forms
from django.utils.translation import ugettext_lazy as _

from ..models import Banners, Paises, BannersUbicacion


class BannersForm(forms.ModelForm): 
    
    pais = forms.ModelMultipleChoiceField(queryset=Paises.objects.all(), widget=forms.SelectMultiple(attrs={ 'class' : 'selectpicker show-menu-arrow form-control required' }))
    ubicacion = forms.ModelMultipleChoiceField(queryset=BannersUbicacion.objects.all(), widget=forms.SelectMultiple(attrs={ 'class' : 'selectpicker show-menu-arrow form-control required' }))

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'pais' or campo == 'ubicacion':
                continue

            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })


    def save(self, commit=True):
        instance = forms.ModelForm.save(self, False)

        old_save_m2m = self.save_m2m
        def save_m2m():
            old_save_m2m()
           
            instance.pais.clear() 
            for pais in self.cleaned_data['pais']:
                instance.pais.add(pais)

            instance.ubicacion.clear() 
            for ubicacion in self.cleaned_data['ubicacion']:
                instance.ubicacion.add(ubicacion)

        self.save_m2m = save_m2m

        if commit:
            instance.save()
            self.save_m2m()

        return instance


    class Meta:
        model = Banners
        fields = ['titulo', 'subtitulo', 'pais', 'ubicacion', 'multimedia', 'posicion', 'activo']
        widgets = {
            #'category': forms.SelectMultiple(attrs={ 'class' : 'selectpicker show-menu-arrow form-control required' })
        }

