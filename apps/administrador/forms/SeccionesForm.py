from django import forms

from ..models import Secciones


class SeccionesForm(forms.ModelForm):
    
    class Meta:
        model = Secciones
        fields = ['nombre']
