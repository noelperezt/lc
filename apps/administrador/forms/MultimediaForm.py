from django import forms
from django.utils.translation import ugettext_lazy as _

from ..models import Multimedia


class MultimediaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

    class Meta:
        model = Multimedia
        fields = ['email', 'pais', 'ciudad', 'direccion', 'iframe_ubicacion']
