from django import forms

from ..models import Testimonios, Paises


class TestimoniosForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'pais':
                continue

            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })
        
        self.fields['pais'].widget.attrs.update({'class' : 'form-control campo selectpicker', 'data-live-search':'true'})

    class Meta:

        model = Testimonios
        fields = ['pais', 'nombre', 'apellido', 'email', 'testimonial', 'archivo', 'activo']
        widgets = {
            'testimonial'    : forms.Textarea(attrs={'class': 'form-control campo', 'rows': 5, 'cols': 200, 'style': 'resize:none;'})
        }

