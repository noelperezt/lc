from django import forms

from ..models import EstudiaTrabaja, Paises


class EstudiaTrabajaForm(forms.ModelForm):

    pais = forms.ModelMultipleChoiceField(queryset=Paises.objects.all(), widget=forms.SelectMultiple(attrs={'class' : 'selectpicker','required':'required','multiple':'multiple','data-live-search':'true','data-width':'100%'}))
    #ciudad = forms.ModelChoiceField(queryset=Ciudades.objects.none(), empty_label=_('- Seleccione una Ciudad'))
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'pais':
                continue
                
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['descripcion'].widget.attrs.update({'class' : 'textareaTiny'})


    def save(self, commit=True):
        instance = forms.ModelForm.save(self, False)

        old_save_m2m = self.save_m2m
        def save_m2m():
            old_save_m2m()
           
            instance.pais.clear() 
            for pais in self.cleaned_data['pais']:
                instance.pais.add(pais)

        self.save_m2m = save_m2m

        if commit:
            instance.save()
            self.save_m2m()

        return instance
        

    class Meta:
        model = EstudiaTrabaja
        fields = ['pais', 'descripcion', 'titulo']
        