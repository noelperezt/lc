from django import forms

from ..models import OportunidadEmpleoPostulados, Paises


class OportunidadEmpleoPostuladosForm(forms.ModelForm):

    pais = forms.ModelMultipleChoiceField(queryset=Paises.objects.all(), widget=forms.SelectMultiple(attrs={'class' : 'selectpicker','required':'required','multiple':'multiple','data-live-search':'true','data-width':'100%'}))


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'pais':
                continue

            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })
    

    class Meta:
        model = OportunidadEmpleoPostulados
        fields = ['pais', 'vacante', 'oficina', 'nombre', 'apellido', 'email']
        widgets = {
            'descripcion'    : forms.Textarea(attrs={'class': 'form-control campo', 'rows': 5, 'cols': 200, 'style': 'resize:none;'})
        }
