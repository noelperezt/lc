from django import forms

from ..models import QuienesSomos


class Quienes_somosForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['resena'].widget.attrs.update({'class' : 'textareaTiny'})
        self.fields['mision'].widget.attrs.update({'class' : 'textareaTiny'})
        self.fields['vision'].widget.attrs.update({'class' : 'textareaTiny'})
        

    class Meta:
        model = QuienesSomos
        fields = ['titulo', 'vision', 'mision', 'resena', 'multimedia']
        widgets = {
            'vision': forms.Textarea(attrs={'class': 'form-control campo', 'required':'required', 'rows': 5, 'style': 'resize:none;'}),
            'mision': forms.Textarea(attrs={'class': 'form-control campo', 'required':'required', 'rows': 5, 'style': 'resize:none;'})
        }