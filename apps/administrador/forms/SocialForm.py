from django import forms

from ..models import Social


class SocialForm(forms.ModelForm):
    class Meta:
        model = Social
        fields = ['nombre', 'pais', 'ubicacion', 'archivo', 'posicion', 'activo']
