from django import forms

from ..models import Paises, Ciudades

class PaisForm(forms.ModelForm):

    class Meta:
        model = Paises
        fields = ['pais']


class CiudadForm(forms.ModelForm):

    pais = forms.ModelChoiceField(queryset=Paises.objects.all())
    ciudad = forms.ModelChoiceField(queryset=Ciudades.objects.none())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['pais'].widget.attrs.update({'class' : 'form-control campo selectpicker','data-live-search':'true','data-width':'100%'});
        self.fields['ciudad'].widget.attrs.update({'class' : 'form-control campo selectpicker','data-live-search':'true','data-width':'100%'});

    class Meta:
        model = Ciudades
        fields = ['pais', 'ciudad']
