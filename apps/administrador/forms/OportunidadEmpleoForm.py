from django import forms

from ..models import OportunidadEmpleo, Paises


class OportunidadEmpleoForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['descripcion'].widget.attrs.update({'class' : 'textareaTiny'})
        self.fields['vacante'].widget.attrs.update({'class' : 'textareaTiny'})


    class Meta:
        model = OportunidadEmpleo
        fields = ['oficina', 'vacante', 'descripcion']
        widgets = {
            'descripcion'    : forms.Textarea(attrs={'class': 'form-control campo', 'rows': 5, 'cols': 200, 'style': 'resize:none;'})
        }
