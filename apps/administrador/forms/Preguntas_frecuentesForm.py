from django import forms

from ..models import PreguntasFrecuentes


class Preguntas_frecuentesForm(forms.ModelForm):

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            if campo == 'secciones':
                continue

            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['secciones'].widget.attrs.update({'class' : 'form-control campo selectpicker'})


    class Meta:
        model = PreguntasFrecuentes
        fields = ['secciones', 'pregunta', 'respuesta']