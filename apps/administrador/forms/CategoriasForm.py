from django import forms

from ..models import Categorias


class CategoriasForm(forms.ModelForm):

    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

    class Meta:
        model = Categorias
        fields = ['nombre']
