# coding=utf-8
# el elemento 'permisos' solo tiene como funcion referenciar en el arbol de permisos todasview_ las urls de la vistas de dicha opcion

MENU = [
    {
        'nombre' : 'Inicio',
        'direccion' : 'administrador:index',
        'icono' : 'fa fa-home'
    },
    {
        'nombre' : 'Administrador',
        'icono' : 'fa fa-cogs',
        'menu' : [
            {
                'nombre' : 'Usuarios',
                'direccion' : 'administrador:usuarios',
                'icono' : 'fa fa-user',
                'permisos' : ['view_usuarios', 'detail_usuarios', 'add_usuarios', 'change_usuarios', 'delete_usuarios']
            },
            {
                'nombre' : 'Perfiles',
                'direccion' : 'administrador:perfiles',
                'icono' : 'fa fa-users',
                'permisos' : ['add_group', 'change_group', 'delete_group']
            }
        ]
    },
    {
        'nombre' : 'Instituciones',
        'direccion' : 'administrador:instituciones',
        'icono' : 'fa fa-building',
        'permisos' : ['view_institucion', 'detail_institucion', 'add_institucion', 'change_institucion', 'delete_institucion']
    },
    # {
    #     'nombre' : 'Cursos y Programas',
    #     'icono' : 'fa fa-graduation-cap',
    #     'permisos' : ['view_cursos_categorias', 'detail_cursos_categorias', 'add_cursos_categorias', 'change_cursos_categorias', 'delete_cursos_categorias']
    # },
    {
        'nombre' : 'Vive y Trabaja',
        'icono' : 'fa fa-book',
        'menu':[
                {
                    'nombre' : 'Alojamientos',
                    'direccion' : 'administrador:alojamientos',
                    'icono' : 'fa fa-building-o',
                    'permisos' : ['view_inmuebles', 'detail_inmuebles', 'add_inmuebles', 'change_inmuebles', 'delete_inmuebles']
                },
                {
                    'nombre' : 'Expo Eventos',
                    'direccion' : 'administrador:eventos',
                    'icono' : 'fa fa-calendar',
                    'permisos' : ['view_expo_eventos', 'detail_expo_eventos', 'add_expo_eventos', 'change_expo_eventos', 'delete_expo_eventos']
                },
                {
                    'nombre' : 'Estudia y Trabaja',
                    'direccion' : 'administrador:estudia-trabaja',
                    'icono' : 'fa fa-book',
                    'permisos' : ['view_estudiatrabaja', 'detail_estudiatrabaja', 'add_estudiatrabaja', 'change_estudiatrabaja', 'delete_estudiatrabaja']
                },
            ]
    },
    {
        'nombre' : 'Servicios',
        'direccion' : 'administrador:servicios',
        'icono' : 'fa fa-bell-o',
        'permisos' : ['view_servicios', 'detail_servicios', 'add_servicios', 'change_servicios', 'delete_servicios']
    },
    {
        'nombre' : 'Sobre nosotros',
        'icono' : 'fa fa-book',
        'menu':[
            {
                'nombre' : 'Quienes somos',
                'direccion' : 'administrador:quienes_somos',
                'icono' : 'fa fa-street-view',
                'permisos' : ['view_quienessomos', 'detail_quienessomos', 'add_quienessomos', 'change_quienessomos', 'delete_quienessomos']
            },
            {
                'nombre' : 'Responsabilidad social',
                'direccion' : 'administrador:responsabilidad_social',
                'icono' : 'fa fa-calendar',
                'permisos' : ['view_responsabilidadsocial', 'detail_responsabilidadsocial', 'add_responsabilidadsocial', 'change_responsabilidadsocial', 'delete_responsabilidadsocial']
            },
            {
                'nombre' : 'Agradecimientos',
                'direccion' : 'administrador:agradecimientos',
                'icono' : 'fa fa-book',
                'permisos' : ['view_agradecimientos', 'detail_agradecimientos', 'add_agradecimientos', 'change_agradecimientos', 'delete_agradecimientos']
            }
        ]
    },
    {
        'nombre' : 'Contactos',
        'icono' : 'fa fa-address-card-o',
        'menu':[
            {
                'nombre' : 'Oficinas',
                'direccion' : 'administrador:oficinas',
                'icono' : 'fa fa-street-view',
                'permisos' : ['view_oficinas', 'detail_oficinas', 'add_oficinas', 'change_oficinas', 'delete_oficinas']
            },
            {
                'nombre' : 'Oportunidad de Empleo',
                'direccion' : 'administrador:oportunidad_empleo',
                'icono' : 'fa fa-calendar',
                'permisos' : ['view_oportunidadempleo', 'detail_oportunidadempleo', 'add_oportunidadempleo', 'change_oportunidadempleo', 'delete_oportunidadempleo']
            },
            {
                'nombre' : 'Postulados a Vacantes',
                'direccion' : 'administrador:oportunidad_empleo_postulados',
                'icono' : 'fa fa-calendar',
                'permisos' : ['view_oportunidadempleopostulados', 'detail_oportunidadempleopostulados', 'add_oportunidadempleopostulados', 'change_oportunidadempleopostulados', 'delete_oportunidadempleopostulados']
            },
            {
                'nombre' : 'Preguntas Frecuentes',
                'direccion' : 'administrador:preguntas_frecuentes',
                'icono' : 'fa fa-book',
                'permisos' : ['view_preguntasfrecuentes', 'detail_preguntasfrecuentes', 'add_preguntasfrecuentes', 'change_preguntasfrecuentes', 'delete_preguntasfrecuentes']
            },
            {
                'nombre' : 'Solicitud de Información',
                'direccion' : 'administrador:informacion',
                'icono' : 'fa fa-info',
                'permisos' : ['view_informacion', 'detail_informacion', 'add_informacion', 'change_informacion', 'delete_informacion']
            },
            {
                'nombre' : 'Sugerencias',
                'direccion' : 'administrador:sugerencias',
                'icono' : 'fa fa-commenting',
                'permisos' : ['view_sugerencias', 'detail_sugerencias', 'add_sugerencias', 'change_sugerencias', 'delete_sugerencias']
            }

        ]
    },
    {
        'nombre' : 'Administrador de banners',
        'direccion' : 'administrador:banners',
        'icono' : 'fa fa-window-maximize',
        'permisos' : ['view_banners', 'detail_banners', 'add_banners', 'change_banners', 'delete_banners']
    },
    {
        'nombre' : 'Testimoniales',
        'direccion' : 'administrador:testimonios',
        'icono' : 'fa fa-newspaper-o',
        'permisos' : ['view_testimonios', 'detail_testimonios', 'add_testimonios', 'change_testimonios', 'delete_testimonios']
    },
    {
        'nombre' : 'Ubicación y Categorías',
        'icono' : 'fa fa-tasks',
        'menu':[
            {
                'nombre' : 'Ubicacion',
                'direccion' : 'administrador:ubicacion',
                'icono' : 'fa fa-street-view',
                'permisos' : ['view_paises', 'detail_paises', 'add_paises', 'change_paises', 'delete_paises']
            },
            {
                'nombre' : 'Categorías',
                'direccion' : 'administrador:categorias',
                'icono' : 'fa fa-tasks',
                'permisos' : ['view_categorias', 'detail_categorias', 'add_categorias', 'change_categorias', 'delete_categorias']
            }
        ]
    },
    {
        'nombre' : 'Noticias',
        'direccion' : 'administrador:noticiaadmin',
        'icono' : 'fa fa-newspaper-o',
        'permisos' : ['view_noticiaadmin', 'detail_noticiaadmin', 'change_noticiaadmin', 'delete_noticiaadmin']
    },
]