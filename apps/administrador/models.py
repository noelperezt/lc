import os
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from sorl.thumbnail import ImageField


def path_and_rename(path, filename):
    file_root, file_ext = os.path.splitext(filename)
    filename = '{}{}'.format(get_random_string(20).lower(), file_ext)
    
    return os.path.join(path, filename)


class Modelo(models.Model):

    class Meta:
        abstract            = True
        get_latest_by       = 'updated_at'
        ordering            = ('-updated_at', '-created_at',)
        default_permissions = ('add', 'change', 'delete', 'view')


class Paises(models.Model):
    id = models.CharField(max_length=3, primary_key=True, verbose_name=_('Codigo pais'))
    pais = models.CharField(max_length=60, null=False, blank=False,verbose_name=_('Pais'))
    #region = models.CharField(max_length=30, null=False, blank=False, verbose_name=_('Region'))

    def __str__(self):
        return self.pais

    class Meta:
        db_table            = 'paises'
        verbose_name        = _('Pais')
        verbose_name_plural = _('Paises')
        get_latest_by       = 'pais'
        ordering            = ('pais',)

        permissions = (
            ("view_paises", "Can view Pais"),
            ("detail_paises", "Can detail Pais"),
        )

"""
class Estados(models.Model):
    estado = models.CharField(max_length=60, null=False, blank=False,verbose_name=_('Estado'))
    pais = models.ForeignKey(Paises, on_delete=models.CASCADE)

    class Meta:
        db_table            = 'estados'
        verbose_name        = _('Estado')
        verbose_name_plural = _('Estados')
        get_latest_by       = 'estado'
        ordering            = ('estado',)

        permissions = (
            ("view_estados", "Can view Estado"),
            ("detail_estados", "Can detail Estado"),
        )
"""

class Ciudades(models.Model):
    ciudad = models.CharField(max_length=60, null=False, blank=False, verbose_name=_('Ciudad'))
    #estado = models.ForeignKey(Estados, on_delete=models.CASCADE)
    pais = models.ForeignKey(Paises, on_delete=models.CASCADE)

    def __str__(self):
        return self.ciudad

    class Meta:
        db_table            = 'ciudades'
        verbose_name        = _('Ciudad')
        verbose_name_plural = _('Ciudades')
        get_latest_by       = 'ciudad'
        ordering            = ('ciudad',)

        permissions = (
            ("view_ciudades", "Can view Ciudad"),
            ("detail_ciudades", "Can detail Ciudad"),
        )


class UsuariosManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given username must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Usuarios(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'
    #REQUIRED_FIELDS = ['dni', 'genero', 'nacimiento', 'telefono']

    GENERO_CHOICES = (
        ('m', _('Masculino')),
        ('f', _('Femenino')),
    )
    TIPO_USUARIO = (
        ('LC', 'LC'),
        ('E', _('Estudiante')),
        ('I', _('Institucion educativa')),
        ('A', _('Arrendatarios')),
    )

    def rename_foto(self, filename):
        return path_and_rename("usuarios", filename)

    nombre       = models.CharField(max_length=100, null=True, blank=True, verbose_name=_('Nombre'))
    apellido     = models.CharField(max_length=100, null=True, blank=True, verbose_name=_('Apellido'))
    email        = models.EmailField(max_length=80, null=False, blank=False, db_index=True, unique=True, verbose_name =_('Correo Electronico'))

    # documento nacional de identidad
    dni          = models.CharField(max_length=15, null=True, blank=True, db_index=True, unique=True, verbose_name=_('Documento de Identidad'))
    genero       = models.CharField(choices=GENERO_CHOICES, max_length=15, null=True, blank=True, verbose_name=_('Genero'))
    nacimiento   = models.DateField(null=True, blank=True, verbose_name=_('Fecha de Nacimiento'))
    direccion    = models.TextField(max_length=250, null=True, blank=True, verbose_name=_('Dirección'))
    telefono     = models.CharField(max_length=20, null=True, blank=True, verbose_name=_('Telefono'))
    
    foto         = models.FileField(max_length=200, null=True, blank=True, upload_to=rename_foto, verbose_name=_('Imagen'))
    pais         = models.ForeignKey(Paises, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Pais de Residencia'))
    tipo_usuario = models.CharField(max_length=2, default='LC')
    activo       = models.BooleanField(_('Activo'), default=False)
    #username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()

    #username = models.CharField(
    #    _('username'),
    #    max_length=150,
    #    help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
    #    validators=[username_validator],
    #)

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    objects = UsuariosManager()

    def __str__(self):
        return str(self.nombre) + ' ' + str(self.apellido)

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def get_email_user(self):
        """
        Retorna el correo para que sea usado posteriormente
        """
        return self.email

    class Meta:
        db_table = 'usuarios'
        verbose_name = _('Usuario')
        verbose_name_plural = _('Usuarios')

        permissions = (
            ("view_usuarios", "Can view Usuario"),
            ("detail_usuarios", "Can detail Usuario"),
        )


class Multimedia(models.Model):
    
    archivo           = models.CharField(null=False, blank=False, max_length=80)
    nombre_archivo    = models.CharField(null=False, blank=False, max_length=80)
    sm                = models.CharField(null=False, blank=False, max_length=80)
    md                = models.CharField(null=False, blank=False, max_length=80)
    lg                = models.CharField(null=False, blank=False, max_length=80)
    tipo_archivo      = models.CharField(null=False, blank=False, max_length=1)
    titulo            = models.CharField(null=False, blank=False, max_length=120)
    texto_alternativo = models.CharField(null=False, blank=False, max_length=100)
    path              = models.CharField(null=False, blank=False, max_length=100, verbose_name=_('Nombre directorio'))
    
    created_at        = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at        = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre_archivo

    # reescribir el metodo save para dejar toda la parte de multimedia lista para que sea usada por las views y los
    #templates
    #def save(self, *args, **kwargs):
    #    pass

    class Meta:
        db_table            = 'multimedia'
        verbose_name        = _('Multimedia')
        verbose_name_plural = _('Multimedias')


class Categorias(Modelo):

    nombre     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    categoria  = models.ForeignKey("self", on_delete=models.DO_NOTHING)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'categorias'
        verbose_name        = _('Categoria')
        verbose_name_plural = _('Categorias')

        permissions = (
            ("view_categorias", "Can view Categoria"),
            ("detail_categorias", "Can detail Categoria"),
        )


class BannersUbicacion(Modelo):

    nombre     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'bannersubicacion'
        verbose_name        = _('Ubicación de banner')
        verbose_name_plural = _('Ubicación de banners')


class Banners(Modelo):

    def rename_archivo(self, filename):
        return path_and_rename("banners", filename)

    pais       = models.ManyToManyField(Paises)
    ubicacion  = models.ManyToManyField(BannersUbicacion)
    #nombre     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    titulo     = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Titulo'))
    subtitulo  = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Subtitulo'))
    #archivo    = models.FileField(null=False, blank=False, upload_to=rename_archivo, verbose_name=_('Archivo'))
    multimedia = models.ManyToManyField(Multimedia)
    posicion   = models.PositiveIntegerField(null=False, blank=False, verbose_name=_('Posición'))
    activo     = models.BooleanField(null=False, blank=False, verbose_name=_('Activo'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'banners'
        verbose_name        = _('Banner')
        verbose_name_plural = _('Banners')

        permissions = (
            ("view_banners", "Can view Banner"),
            ("detail_banners", "Can detail Banner"),
        )


class EstudiaTrabaja(Modelo):

    pais        = models.ManyToManyField(Paises)
    titulo      = models.CharField(max_length=200, null=False, blank=False, verbose_name=_('Titulo'))
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    multimedia  = models.ManyToManyField(Multimedia)
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'estudia_trabaja'
        verbose_name        = _('Estudia Trabaja')
        verbose_name_plural = _('Estudia Trabaja')

        permissions = (
            ("view_estudiatrabaja", "Can view Estudia Trabaja"),
            ("detail_estudiatrabaja", "Can detail Estudia Trabaja"),
        )


class TipoServicio(Modelo):

    nombre     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre de Sección'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'tipo_servicio'
        verbose_name        = _('Tipo de Servicio')
        verbose_name_plural = _('Tipos de Servicios')

        permissions = (
            ("view_tipo_servicio", "Can view Tipo de Servicio"),
            ("detail_tipo_servicio", "Can detail Tipo de Servicio"),
        )


class Servicios(Modelo):
    """Servicios que ofrece el inmueble Ej. WiFi, mascotas, baños, etc"""
    def rename_archivo(self, filename):
        return path_and_rename("servicios", filename)

    tipo_servicio = models.ManyToManyField(TipoServicio, verbose_name=_('Tipo de Servicio'))
    activo      = models.BooleanField(null=False, blank=False, verbose_name=_('Activo'))
    archivo     = models.FileField(null=True, blank=True, upload_to=rename_archivo, verbose_name=_('Archivo'))
    #multimedia  = models.ManyToManyField(Multimedia)
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'), default='')
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))
    

    def __str__(self):
        return self.nombre_servico

    class Meta:
        db_table            = 'servicios'
        verbose_name        = _('Servicio')
        verbose_name_plural = _('Servicios')

        permissions = (
            ("view_servicios", "Can view Servicio"),
            ("detail_servicios", "Can detail Servicio"),
        )


class Servicios_lc(Modelo):
    
    nombre      = models.CharField(max_length=60)    
    url         = models.SlugField(max_length=120, null=False, blank=False)
    multimedia  = models.ManyToManyField(Multimedia)
    activo      = models.BooleanField(null=False, blank=False, verbose_name=_('Activo'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'servicios_lc'
        verbose_name        = _('Servicio_lc')
        verbose_name_plural = _('Servicios_lc')

        permissions = (
            ("view_servicios_lc", "Can view Servicio_lc"),
            ("detail_servicios_lc", "Can detail Servicio_lc"),
        )

class Servicios_lc_pais(Modelo):
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    servicios_lc = models.ForeignKey(Servicios_lc)
    pais = models.ForeignKey(Paises)
    

    class Meta:
        db_table            = 'servicios_lc_pais'
        verbose_name        = _('Servicios lc por pais')
        verbose_name_plural = _('Servicios lc por paises')

class Secciones(Modelo):
    
    # Instituciones
    # Alojaminetos
    # Servicios
    # Cursos / Programas
    nombre     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre de Sección'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'secciones'
        verbose_name        = _('Sección')
        verbose_name_plural = _('Secciones')

        permissions = (
            ("view_secciones", "Can view Sección"),
            ("detail_secciones", "Can detail Sección"),
        )
        

class Testimonios(Modelo):
    """
    TIPO = (
        ('I', _('Instituciones')),
        ('A', _('Alojaminetos')),
        ('S', _('Servicios')),
        ('C', _('Cursos / Programas'))
    )
    secciones   = models.CharField(max_length=1, null=False, blank= False, db_index= True, choices=TIPO, verbose_name=_('Sección'))
    """

    def rename_archivo(self, filename):
        return path_and_rename("testimonios", filename)

    pais        = models.ForeignKey(Paises, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Pais de Residencia'))
    
    secciones   = models.ForeignKey(Secciones, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Secciones de la Pagina'))
    nombre      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    apellido    = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Apellido'))
    email       = models.EmailField(max_length=80, null=True, blank=True, verbose_name=_('Correo Electronico'))
    testimonial = models.TextField(null=False, blank=False, verbose_name=_('Testimonio'))
    
    archivo     = models.FileField(null=True, blank=True, upload_to=rename_archivo, verbose_name=_('Archivo'))
    #multimedia  = models.ManyToManyField(Multimedia)
    activo      = models.BooleanField(null=False, blank=False, verbose_name=_('Activo'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))
    

    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'testimonios'
        verbose_name        = _('Testimonio')
        verbose_name_plural = _('Testimonios')

        permissions = (
            ("view_testimonios", "Can view Testimonio"),
            ("detail_testimonios", "Can detail Testimonio"),
        )


class QuienesSomos(Modelo):

    def rename_archivo(self, filename):
        return path_and_rename("quienes_somos", filename)

    titulo     = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Titulo'))
    vision     = models.TextField(null=False, blank=False, verbose_name=_('Visión'))
    mision     = models.TextField(null=False, blank=False, verbose_name=_('Misión'))
    resena     = models.TextField(null=False, blank=False, verbose_name=_('Reseña'))
    
    #archivo    = models.FileField(null=True, blank=True, upload_to=rename_archivo, verbose_name=_('Archivo'))
    multimedia  = models.ManyToManyField(Multimedia)
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.titulo

    class Meta:
        db_table            = 'quienes_somos'
        verbose_name        = _('Quienes Somos')
        verbose_name_plural = _('Quienes Somos')

        permissions = (
            ("view_quienessomos", "Can view Quienes Somos"),
            ("detail_quienessomos", "Can detail Quienes Somos"),
        )

class ResponsabilidadSocial(Modelo):

    def rename_archivo(self, filename):
        return path_and_rename("responsabilidad_social", filename)

    pais        = models.ManyToManyField(Paises)
    titulo      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Titulo'))
    #archivo     = models.FileField(null=True, blank=True, upload_to=rename_archivo, verbose_name=_('Archivo'))
    multimedia  = models.ManyToManyField(Multimedia)
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'responsabilidad_social'
        verbose_name        = _('Responsabilidad Social')
        verbose_name_plural = _('Responsabilidades Sociales')

        permissions = (
            ("view_responsabilidadsocial", "Can view Responsabilidad Social"),
            ("detail_responsabilidadsocial", "Can detail Responsabilidad Social"),
        )

class Agradecimientos(Modelo):

    def rename_archivo(self, filename):
        return path_and_rename("agradecimientos", filename)

    pais        = models.ManyToManyField(Paises)
    titulo      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Titulo'))
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    #archivo     = models.FileField(null=True, blank=True, upload_to=rename_archivo, verbose_name=_('Imagen'))
    multimedia  = models.ManyToManyField(Multimedia)
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.nombre

    class Meta:
        db_table            = 'agradecimientos'
        verbose_name        = _('Agradecimiento')
        verbose_name_plural = _('Agradecimientos')

        permissions = (
            ("view_agradecimientos", "Can view Agradecimiento"),
            ("detail_agradecimientos", "Can detail Agradecimiento"),
        )


class Oficinas(Modelo):

    pais             = models.ForeignKey(Paises, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Pais'))
    ciudad           = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    email            = models.EmailField(max_length=80, null=False, blank=False, db_index=True, unique=True, verbose_name=_('Correo Electronico'))
    direccion        = models.TextField(max_length=250, null=True, blank=True, verbose_name=_('Dirección'))
    #iframe_ubicacion = models.CharField(max_length=200, null=False, blank=False)
    iframe_ubicacion = models.URLField(null=False, blank=False, verbose_name=_('Ubicación'))
    multimedia       = models.ManyToManyField(Multimedia)
    
    created_at       = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at       = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.direccion

    class Meta:
        db_table            = 'oficinas'
        verbose_name        = _('Oficina')
        verbose_name_plural = _('Oficinas')

        permissions = (
            ("view_oficinas", "Can view Oficina"),
            ("detail_oficinas", "Can detail Oficina"),
        )


class OficinasTelefonos(Modelo):
    
    oficina = models.ForeignKey(Oficinas, null=False, blank=False, on_delete=models.CASCADE)
    numero  = models.CharField(max_length=20, null=False, blank=False, verbose_name=_('Numero'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))

    def __str__(self):
        return self.numero

    class Meta:
        db_table            = 'oficinas_telefonos'
        verbose_name        = _('Telefono de oficina')
        verbose_name_plural = _('Telefonos de oficinas')


class OportunidadEmpleo(Modelo):

    oficina     = models.ForeignKey(Oficinas, null=False, blank=False, on_delete=models.CASCADE)
    vacante     = models.TextField(null=False, blank=False, verbose_name=_('Vacante'))
    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.descripcion

    class Meta:
        db_table            = 'oportunidad_empleo'
        verbose_name        = _('Oportunidad de Empleo')
        verbose_name_plural = _('Oportunidades de Empleos')

        permissions = (
            ("view_oportunidadempleo", "Can view Oportunidad de Empleo"),
            ("detail_oportunidadempleo", "Can detail Oportunidad de Empleo"),
        )


class OportunidadEmpleoPostulados(Modelo):

    vacante     = models.ForeignKey(OportunidadEmpleo, null=False, blank=False, on_delete=models.CASCADE)
    oficina     = models.ForeignKey(Oficinas, null=False, blank=False, on_delete=models.CASCADE)
    pais        = models.ForeignKey(Paises, null=False, blank=False, on_delete=models.PROTECT)
    nombre      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    apellido    = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Apellido'))
    email       = models.EmailField(max_length=80, null=False, blank=False, db_index=True, unique=True, verbose_name =_('Correo Electronico'))

    descripcion = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))
    
    created_at  = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at  = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.descripcion

    class Meta:
        db_table            = 'oportunidad_empleo_postulados'
        verbose_name        = _('Oportunidad de Empleo Postulados')
        verbose_name_plural = _('Oportunidades de Empleos Postulados')

        permissions = (
            ("view_oportunidadempleopostulados", "Can view Oportunidad de Empleo Postulados"),
            ("detail_oportunidadempleopostulados", "Can detail Oportunidad de Empleo Postulados"),
        )


class PreguntasFrecuentes(Modelo):
    """
    TIPO = (
        ('I', _('Instituciones')),
        ('A', _('Alojaminetos')),
        ('S', _('Servicios')),
        ('C', _('Cursos / Programas'))
    )

    items     = models.CharField(max_length=1, null=False, blank= False, db_index= True, choices=TIPO, verbose_name=_('Sección'))
    """
    secciones = models.ForeignKey(Secciones, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Secciones de la Pagina'))

    pregunta  = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Pregunta'))
    respuesta = models.TextField(null=True, blank=True, verbose_name=_('Respuesta'))
    archivo = models.ForeignKey(Multimedia, on_delete = models.PROTECT, null=False, blank=False)
    
    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.pregunta

    class Meta:
        db_table            = 'preguntas_frecuentes'
        verbose_name        = _('Pregunta Frecuente')
        verbose_name_plural = _('Preguntas Frecuentes')

        permissions = (
            ("view_preguntasfrecuentes", "Can view Pregunta Frecuente"),
            ("detail_preguntasfrecuentes", "Can detail Pregunta Frecuente"),
        )


class Favoritos(Modelo):
    """
    tipo = I (institucion), A (alojamineto), C (cursos y programas)
    TIPO = (
        ('I', _('Institución')),
        ('A', _('Alojamineto')),
        ('C', _('Cursos y Programas'))
    )

    tipo = models.CharField(max_length=1, null=False, blank= False, db_index= True, choices=TIPO)
    """
    #id_tipo = models.BigIntegerField()
    id_usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE)
    secciones  = models.ForeignKey(Secciones, null=True, blank=True, on_delete=models.CASCADE, verbose_name=_('Secciones de la Pagina'))

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.tipo

    class Meta:
        db_table            = 'favoritos'
        verbose_name        = _('Favorito')
        verbose_name_plural = _('Favoritos')

        permissions = (
            ("view_favoritos", "Can view Favorito"),
            ("detail_favoritos", "Can detail Favorito"),
        )


class Informacion(Modelo):

    pais        = models.ForeignKey(Paises, null=False, blank=False, on_delete=models.CASCADE)
    nombre      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    apellido    = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Apellido'))
    email       = models.EmailField(max_length=80, null=False, blank=False, db_index=True, unique=True, verbose_name =_('Correo Electronico'))

    descripcion     = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))

    created_at      = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at      = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.descripcion

    class Meta:
        db_table            = 'informacion'
        verbose_name        = _('Información')
        verbose_name_plural = _('Informaciones')

        permissions = (
            ("view_informacion", "Can view Informacion"),
            ("detail_informacion", "Can detail Informacion"),
        )


class Sugerencias(Modelo):

    pais        = models.ForeignKey(Paises, null=False, blank=False, on_delete=models.CASCADE)
    nombre      = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Nombre'))
    apellido    = models.CharField(max_length=100, null=False, blank=False, verbose_name=_('Apellido'))
    email       = models.EmailField(max_length=80, null=False, blank=False, db_index=True, unique=True, verbose_name =_('Correo Electronico'))

    descripcion     = models.TextField(null=False, blank=False, verbose_name=_('Descripción'))

    created_at      = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    updated_at      = models.DateTimeField(auto_now=True, editable=False, verbose_name=_('Fecha de Actualización'))


    def __str__(self):
        return self.descripcion

    class Meta:
        db_table            = 'sugerencias'
        verbose_name        = _('Sugerencia')
        verbose_name_plural = _('Sugerencias')

        permissions = (
            ("view_sugerencias", "Can view Sugerencia"),
            ("detail_sugerencias", "Can detail Sugerencia"),
        )
