import os
from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils import timezone
import hashlib
from PIL import Image

class File:
    __file = ''
    __fileName = ''
    __folder_culter = ''
    __types_image = ['']
    __types_videos = ['']
    tipo_archivo = ''

    def __init__(self, file):
        self.__file = file

        if file.content_type in self.__types_image:
            folder_cluster = 'images'
            self.tipo_archivo = 'I'
        elif file.content_type in self.__types_videos:
            folder_cluster = 'videos'
            self.tipo_archivo = 'V'



        #valida si el director para el cluster que quieren usar esta creado en el sistema

        if not os.path.isdir(settings.MEDIA_ROOT + '/' + folder_cluster):
            os.mkdir(settings.MEDIA_ROOT + '/' + folder_cluster)

        #crear el cluster de fecha para organizar los archivo por este metodo
        today = datetime.datetime.now()
        cluster_fecha = today.year + '-' + today.month

        if not os.path.isdir(settings.MEDIA_ROOT + '/' + folder_cluster + '/' + cluster_fecha):
            os.mkdir(settings.MEDIA_ROOT + '/' + folder_cluster + '/' + cluster_fecha)

        #este es la ruta donde se guardara el archivo
        self.__folder_culter = settings.MEDIA_ROOT + '/' + folder_cluster + '/' + cluster_fecha + '/'

    def __getRandomName(self):
        return hashlib.sha1(str(str(timezone.now())).encode('utf-8')).hexdigest()

    def __saveImage(self):
        try:
            destination = open(self.__folder_culter + self.__fileName, 'wb+')
            for chunk in self.__file.chunks():
                destination.write(chunk)

            self.__setResize(800)
            self.__setResize(400, 'md')
            self.__setResize(200, 'sm')

            return True

        except (OSError, IOError):
            self.mensaje = _('Disculpe no se logro subir la imagen')

    def upload(self):
        self.__fileName = self.__getRandomName()

        if self.tipo_archivo == 'I':
            self.__fileName + '.png'
            return self.__saveImage()


    def getFile(self):
         return self.__fileName

    def getPathFile(self):
        return self.__folder_culter

    def __setResize(self, width, name=''):
        origin = self.__folder_culter + self.__fileName
        im1 = Image.open(origin)

        wpercent = float(width / im1.size[0])
        hsize = int(im1.size[1] * wpercent)
        img = im1.resize((width, hsize), Image.ANTIALIAS)  # use nearest neighbour

        if name == '':
            new_img = self.__folder_culter + self.__fileName
        else:
            new_img = self.__folder_culter + name + '_' + self.__fileName

        img.save(new_img)

    @staticmethod
    def setDeleteFile(fileName):
        photo = settings.MEDIA_ROOT + '/' + fileName
        thumb = fileName.split('/')
        photoThumb = settings.MEDIA_ROOT + '/' + thumb[0] + '/thumb_' + thumb[1]
        try:
            os.remove(photo)
            os.remove(photoThumb)
            return True
        except OSError:
            return False