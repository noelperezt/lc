# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable
from apps.datatableview.views.base import DatatableView

# Vista Generica
from ..vista import Vista, VistaMultimedia, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.EventosForm import EventosForm

# Modelos
#from ..models import Eventos
from ..models import Sugerencias #mientras esta listo el modelo de Eventos

class EventosView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    #model = Eventos
    model = Sugerencias
    form_class = EventosForm


class Eventos(EventosView, DatatableView):
    template_name = "eventos.html"

    permission_required = ['eventos']

    librerias = ['datatables', 'bootstrap-select', 'file-upload']

    css = ['Eventos.css', 'Multimedia.css']
    js = ['Eventos.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class EventosDetail(EventosView, DetailView):
    permission_required = ['eventos-detail']


class EventosCreate(EventosView, CreateView):
    permission_required = ['eventos-create']


class EventosUpdate(EventosView, UpdateView):
    permission_required = ['eventos-update']


class EventosDelete(EventosView, DeleteView):
    permission_required = ['eventos-delete']


