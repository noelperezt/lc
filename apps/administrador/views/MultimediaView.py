# Dependencias python
import re
import os
import uuid
import time

# Dependencias Django
from django.core.files.base import ContentFile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from LC.settings import STATIC_URL, STATICFILES_DIRS

from PIL import Image, ImageFile

# Dependencias
from braces.views import LoginRequiredMixin
from sorl.thumbnail import ImageField, get_thumbnail

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexView, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Modelos
from ..models import Multimedia


@method_decorator(csrf_exempt, name='dispatch')
class MultimediaView(LoginRequiredMixin, CreateView):
    model = Multimedia

    def post(self, request, *args, **kwargs):
        
        archivo = self.procesar_archivos('files')

        if 'error' in archivo == False:
            return JsonResponse(archivo.error, status=422)

        instance = self.model()

        instance.titulo            = request.POST.get('titulo_subir_multimedia', '')
        instance.texto_alternativo = request.POST.get('alt__subir_multimedia', '')

        instance.archivo        = archivo['archivo']
        instance.nombre_archivo = archivo['nombre_archivo']
        instance.sm             = '' #archivo['sm']
        instance.md             = '' #archivo['md']
        instance.lg             = '' #archivo['lg']
        instance.tipo_archivo   = archivo['tipo_archivo']
        instance.path           = archivo['path']

        if instance.save() == False:
            return JsonResponse(_('Error al procesar el archivo'), status=422)

        return JsonResponse({
            's':            's',
            'msj':          _('Registro Creado Satisfactoriamente'),
            'id':           instance.id,
            'titulo':       instance.titulo,
            'alt':          instance.texto_alternativo,
            'tipo_archivo': instance.tipo_archivo,
            'nombre':       instance.nombre_archivo,
            'archivo':      instance.archivo,
            'path':         instance.path,
            'full':         instance.path + instance.archivo,
        })
    

    def procesar_archivos(self, archivo, *args, **kwargs):
        
        nombre_origen = kwargs.get('nombre_origen', False);

        if (archivo in self.request.FILES):
            archivo = self.request.FILES[archivo]
        else:
           return {
                'error' : _('No se puede leer el archivo suministrado')
            }
        
        #print(archivo.content_type) 

        archivo_nombre = archivo.name
        if nombre_origen == False:
            archivo_nombre = str(uuid.uuid4().hex) + '.' + archivo_nombre.lower().split('.')[-1]

        tipo_archivo = ''
        _ruta = ''

        if re.search(r'\.(gif|jpe?g|png)$', archivo_nombre, re.M|re.I):
            tipo_archivo = 'i'
            _ruta = 'images'
        elif re.search(r'\.(pdf)$', archivo_nombre, re.M|re.I):
            tipo_archivo = 'p'
            _ruta = 'pdf'
        elif re.search(r'\.(mp4|webm)$', archivo_nombre, re.M|re.I):
            tipo_archivo = 'v'
            _ruta = 'video'
        else:
            return {
                'error' : _('La extensión de archivo no es permitida')
            }
        
        ruta = os.path.join(settings.MEDIA_ROOT, _ruta, time.strftime("%Y-%m"))

        try:
            if not os.path.exists(ruta):
                os.makedirs(ruta, exist_ok=True)
        except OSError as e:
            if e.errno == 17:  #el directorio ya existe
                pass
            else:
                print(e)

        file_obj = open(os.path.join(ruta, archivo_nombre), 'wb+')
        contenido = ContentFile(archivo.read())

        for chunk in contenido.chunks():
            file_obj.write(chunk)
        
        file_obj.close()

        regex = re.compile(r"/+", re.IGNORECASE)

        path = settings.MEDIA_URL + '/' + ruta.replace(settings.MEDIA_ROOT, '')
        path = path.replace('\\', '/')
        path = regex.sub('/', path) + '/'

        # TODO: falta buscar la manera de optimizar la imagen
        """
        if re.search(r'\.(jpe?g)$', archivo_nombre, re.M|re.I):
            ImageFile.MAXBLOCK = 2**20

            _nombre = archivo_nombre.lower().split('.')
            _nombre.pop()
            nombre = '.'.join(_nombre)
                
            im = Image.open(os.path.join(settings.MEDIA_ROOT, _ruta, time.strftime("%Y-%m"), archivo_nombre))
            im.save(os.path.join(settings.MEDIA_ROOT, _ruta, time.strftime("%Y-%m"), nombre + '.png'), 'PNG', optimize=True)
            
            os.remove(os.path.join(settings.MEDIA_ROOT, _ruta, time.strftime("%Y-%m"), archivo_nombre))

            archivo_nombre = nombre + '.png'
        """

        if re.search(r'\.(png|jpe?g)$', archivo_nombre, re.M|re.I):
            r = os.path.join(settings.MEDIA_ROOT, _ruta, time.strftime("%Y-%m"))
            
            self.miniatura(r, archivo_nombre, 800, 'lg')
            self.miniatura(r, archivo_nombre, 400, 'md')
            self.miniatura(r, archivo_nombre, 200, 'sm')
            

        return {
            'path' : path,
            'archivo' : archivo_nombre,
            'nombre_archivo' : archivo.name,
            'tipo_archivo' : tipo_archivo,
        }

    def miniatura(self, ruta, archivo, ancho, prefijo):
        im1 = Image.open(ruta + '/' + archivo)

        wpercent = float(ancho / im1.size[0])
        alto = int(im1.size[1] * wpercent)
        img = im1.resize((ancho, alto), Image.ANTIALIAS)
        
        img.save(ruta + '/' + prefijo + '_' + archivo)

        return prefijo + '_' + archivo


@method_decorator(csrf_exempt, name='dispatch')
class MultimediaInfoView(LoginRequiredMixin, DetailView):
    
    model = Multimedia

    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)

        instance = get_object_or_404(self.model.objects, id = id)

        instance.titulo            = request.POST.get('titulo_subir_multimedia', '')
        instance.texto_alternativo = request.POST.get('alt__subir_multimedia', '')

        if instance.save() == False:
            return JsonResponse(_('Error al procesar el archivo'), status=422)

        return JsonResponse({
            's':   's',
            'msj': _('Registro Actualizado Satisfactoriamente')
        })


@method_decorator(csrf_exempt, name='dispatch')
class MultimediaListaView(LoginRequiredMixin, DetailView):
    
    model = Multimedia

    def post(self, request, *args, **kwargs):
        list = self.model.objects.all()
        paginator = Paginator(list, 25) # Show 25 contacts per page

        page = request.POST.get('page')
        try:
            archivos = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            archivos = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            archivos = paginator.page(paginator.num_pages)

            
        return JsonResponse({
            'archivos': archivos
        })