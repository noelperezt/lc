# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.Responsabilidad_socialForm import Responsabilidad_socialForm

# Modelos
from ..models import ResponsabilidadSocial


class Responsabilidad_socialView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = ResponsabilidadSocial
    form_class = Responsabilidad_socialForm


class Responsabilidad_social(Responsabilidad_socialView, IndexDatatableView):
    template_name = "responsabilidad_social.html"

    permission_required = ['view_responsabilidadsocial']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'bootstrap-select', 'tinymce', 'file-upload']

    css = ['Responsabilidad_social.css', 'Multimedia.css']
    js = ['Responsabilidad_social.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'titulo','descripcion',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['titulo','descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class Responsabilidad_socialDetail(Responsabilidad_socialView, DetailView):
    permission_required = ['detail_responsabilidadsocial']

    def get_instance(self, *args, **kwargs):
        instance = super(Responsabilidad_socialDetail, self).get_instance(**kwargs)
        
        #instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class Responsabilidad_socialCreate(Responsabilidad_socialView, CreateView):
    permission_required = ['add_responsabilidadsocial']


class Responsabilidad_socialUpdate(Responsabilidad_socialView, UpdateView):
    permission_required = ['change_responsabilidadsocial']


class Responsabilidad_socialDelete(Responsabilidad_socialView, DeleteView):
    permission_required = ['delete_responsabilidadsocial']

