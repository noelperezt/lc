# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.ServiciosForm import ServiciosForm
from ..forms.TipoServicioForm import TipoServicioForm

# Modelos
from ..models import Servicios, TipoServicio


class ServiciosView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = Servicios
    form_class = ServiciosForm


class Servicios(ServiciosView, IndexDatatableView):
    template_name = "servicios.html"

    permission_required = ['view_servicios']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'icheck', 'tinymce', 'bootstrap-select', 'file-upload']

    css = ['Servicios.css', 'Multimedia.css']
    js = ['Servicios.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'descripcion', 'activo',
            ('comandos', None, 'get_campos'),
        ]
    }


    def get_context_data(self, **kwargs):
        context = super(Servicios, self).get_context_data(**kwargs)
        context['tipo_servicios'] = TipoServicio.objects.all()
        
        return context

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion', 'activo']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class ServiciosDetail(ServiciosView, DetailView):
    permission_required = ['detail_servicios']

    def get_instance(self, *args, **kwargs):
        instance = super(ServiciosDetail, self).get_instance(**kwargs)
        
        instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class ServiciosCreate(ServiciosView, CreateView):
    permission_required = ['add_servicios']


class ServiciosUpdate(ServiciosView, UpdateView):
    permission_required = ['change_servicios']


class ServiciosDelete(ServiciosView, DeleteView):
    permission_required = ['delete_servicios']


class ServiciosTipo(ServiciosView, CreateView):
    permission_required = ['delete_servicios']

    model = TipoServicio
    form_class = TipoServicioForm


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)

        if not form.is_valid():
            return JsonResponse(form.errors, status=422)
        
        tipo_servicios = form.save()

        return self.response({
            'id' : tipo_servicios.id,
            'nombre' : tipo_servicios.nombre
        })

