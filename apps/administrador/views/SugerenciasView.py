# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.SugerenciasForm import SugerenciasForm

# Modelos
from ..models import Sugerencias


class SugerenciasView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Sugerencias
    form_class = SugerenciasForm


class Sugerencias(SugerenciasView, IndexDatatableView):
    template_name = "sugerencias.html"

    permission_required = ['view_sugerencias']

    librerias = ['datatables', 'bootstrap-select']

    css = ['Sugerencias.css']
    js = ['Sugerencias.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class SugerenciasDetail(SugerenciasView, DetailView):
    permission_required = ['detail_sugerencias']


class SugerenciasCreate(SugerenciasView, CreateView):
    permission_required = ['add_sugerencias']


class SugerenciasUpdate(SugerenciasView, UpdateView):
    permission_required = ['change_sugerencias']


class SugerenciasDelete(SugerenciasView, DeleteView):
    permission_required = ['delete_sugerencias']

