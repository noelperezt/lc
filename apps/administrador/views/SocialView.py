# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.SocialForm import SocialForm

# Modelos
from ..models import Social


class SocialView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Social
    form_class = SocialForm


class Social(SocialView, IndexDatatableView):
    template_name = "social.html"

    permission_required = ['view_social']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'bootstrap-select', 'tinymce']

    css = ['Social.css']
    js = ['Social.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'nombre', 'pais', 'ubicacion', 'posicion', 'activo',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['nombre', 'pais', 'ubicacion', 'posicion', 'activo']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class SocialDetail(SocialView, DetailView):
    permission_required = ['detail_social']


class SocialCreate(SocialView, CreateView):
    permission_required = ['add_social']


class SocialUpdate(SocialView, UpdateView):
    permission_required = ['change_social']


class SocialDelete(SocialView, DeleteView):
    permission_required = ['delete_social']

