# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.OportunidadEmpleoPostuladosForm import OportunidadEmpleoPostuladosForm

# Modelos
from ..models import OportunidadEmpleoPostulados


class Oportunidad_empleo_postuladosView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = OportunidadEmpleoPostulados
    form_class = OportunidadEmpleoPostuladosForm


class Oportunidad_empleo_postulados(Oportunidad_empleo_postuladosView, IndexDatatableView):
    template_name = "oportunidad_empleo_postulados.html"

    permission_required = ['view_oportunidadempleopostulados']

    librerias = ['datatables', 'bootstrap-select']

    css = ['Oportunidad_empleo_postulados.css']
    js = ['Oportunidad_empleo_postulados.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class Oportunidad_empleo_postuladosDetail(Oportunidad_empleo_postuladosView, DetailView):
    permission_required = ['detail_oportunidadempleopostulados']


class Oportunidad_empleo_postuladosCreate(Oportunidad_empleo_postuladosView, CreateView):
    permission_required = ['add_oportunidadempleopostulados']


class Oportunidad_empleo_postuladosUpdate(Oportunidad_empleo_postuladosView, UpdateView):
    permission_required = ['change_oportunidadempleopostulados']


class Oportunidad_empleo_postuladosDelete(Oportunidad_empleo_postuladosView, DeleteView):
    permission_required = ['delete_oportunidadempleopostulados']

