# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.EstudiaTrabajaForm import EstudiaTrabajaForm

# Modelos
from ..models import EstudiaTrabaja


class EstudiaTrabajaView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = EstudiaTrabaja
    form_class = EstudiaTrabajaForm


class EstudiaTrabaja(EstudiaTrabajaView, IndexDatatableView):
    template_name = "estudia_trabaja.html"

    permission_required = ['view_estudiatrabaja']

    librerias = ['datatables', 'tinymce', 'bootstrap-select', 'file-upload']

    css = ['EstudiaTrabaja.css', 'Multimedia.css']
    js = ['EstudiaTrabaja.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'titulo',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['titulo'] 

        def get_campos(self, instance, *args, **kwargs):
            return ''


class EstudiaTrabajaDetail(EstudiaTrabajaView, DetailView):
    permission_required = ['detail_estudiatrabaja']


    def get_instance(self, *args, **kwargs):
        instance = super(EstudiaTrabajaDetail, self).get_instance(**kwargs)
        
        #instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class EstudiaTrabajaCreate(EstudiaTrabajaView, CreateView):
    permission_required = ['add_estudiatrabaja']


class EstudiaTrabajaUpdate(EstudiaTrabajaView, UpdateView):
    permission_required = ['change_estudiatrabaja']


class EstudiaTrabajaDelete(EstudiaTrabajaView, DeleteView):
    permission_required = ['delete_estudiatrabaja']

