# Dependencias Django
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexView, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.OficinasForm import OficinasForm

# Modelos
from ..models import Oficinas, Paises, Ciudades


class OficinasView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = Oficinas
    form_class = OficinasForm


class Oficinas(OficinasView, IndexDatatableView):
    template_name = "oficinas.html"

    permission_required = ['view_oficinas']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'bootstrap-select', 'tinymce', 'file-upload']

    css = ['Oficinas.css', 'file-upload.css', "Multimedia.css"]
    js = ['Oficinas.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'email', 'pais', 'ciudad', 'direccion',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['email', 'pais', 'ciudad', 'direccion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class OficinasDetail(OficinasView, DetailView):
    permission_required = ['detail_oficinas']

    def get(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)
        model = self.model.objects.get(id = id)
        
        #usuarios = persona.objects.all()
        #data = serializers.serialize("json", usuarios)

        ciudad = '' if model.ciudad is None else model.ciudad.id

        if model.pais is None:
            pais = ''
        else:
            pais = model.pais.id
            ciudades = Ciudades.objects.filter(pais_id=pais).order_by('-ciudad')
            ciudad = {'_' : ciudad}

            for c in ciudades:
                ciudad[c.id] = c.ciudad

        return JsonResponse({
            'id'        : id,
            'email'     : model.email,
            'pais'      : pais,
            'ciudad'    : ciudad,
            'telefono'  : model.telefono,
            'direccion' : model.direccion,
            'iframe_ubicacion': model.iframe_ubicacion
        })


class OficinasCiudades(OficinasView, DetailView):
    permission_required = ['detail_oficinas']

    def get(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', '')
        ciudades = Ciudades.objects.filter(pais_id=id).order_by('-ciudad')
        resultado = {'_' : ''}

        for ciudad in ciudades:
            resultado[ciudad.id] = ciudad.ciudad
        
        return JsonResponse({
            'id_ciudad' : resultado
        })


class OficinasCreate(OficinasView, CreateView):
    permission_required = ['add_oficinas']


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        form.fields["ciudad"].queryset = Ciudades.objects.filter(pais_id=request.POST['pais'])

        if not form.is_valid():
            return JsonResponse(form.errors, status=400)
        
        form.save()

        return self.response()


class OficinasUpdate(OficinasView, UpdateView):
    permission_required = ['change_oficinas']

    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)

        instance = get_object_or_404(self.model.objects, id = id)
        form = self.form_class(request.POST, instance = instance)
        form.fields["ciudad"].queryset = Ciudades.objects.filter(pais_id=request.POST['pais'])
        
        if not form.is_valid():
            return JsonResponse(form.errors, status=400)

        instance = form.save()

        return self.response()



class OficinasDelete(OficinasView, DeleteView):
    permission_required = ['delete_oficinas']


class OficinasMutimedia(OficinasView, IndexView):
    permission_required = ['view_oficinas']


    def post(self, request, *args, **kwargs):
        print(request)
