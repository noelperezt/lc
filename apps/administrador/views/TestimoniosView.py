# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.TestimoniosForm import TestimoniosForm

# Modelos
from ..models import Testimonios


class TestimoniosView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Testimonios
    form_class = TestimoniosForm


class Testimonios(TestimoniosView, IndexDatatableView):
    template_name = "testimonios.html"

    permission_required = ['view_testimonios']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'icheck', 'bootstrap-select']

    css = ['Testimonios.css']
    js = ['Testimonios.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'nombre', 'apellido', 'email', 'activo',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['nombre', 'apellido', 'email', 'activo']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class TestimoniosDetail(TestimoniosView, DetailView):
    permission_required = ['detail_testimonios']


    def get_instance(self, *args, **kwargs):
        instance = super(TestimoniosDetail, self).get_instance(**kwargs)
        
        instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class TestimoniosCreate(TestimoniosView, CreateView):
    permission_required = ['add_testimonios']


class TestimoniosUpdate(TestimoniosView, UpdateView):
    permission_required = ['change_testimonios']


class TestimoniosDelete(TestimoniosView, DeleteView):
    permission_required = ['delete_testimonios']

