# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.Preguntas_frecuentesForm import Preguntas_frecuentesForm

# Modelos
from ..models import PreguntasFrecuentes


class Preguntas_frecuentesView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = PreguntasFrecuentes
    form_class = Preguntas_frecuentesForm


class Preguntas_frecuentes(Preguntas_frecuentesView, IndexDatatableView):
    template_name = "preguntas_frecuentes.html"

    permission_required = ['view_preguntasfrecuentes']

    librerias = ['datatables', 'bootstrap-select', 'tinymce', 'file-upload']

    css = ['Preguntas_frecuentes.css', 'Multimedia.css']
    js = ['Preguntas_frecuentes.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'pregunta', 'respuesta',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Acciones", sources=None, processor='get_campos')

        class Meta:
            columns = ['pregunta', 'respuesta',]

        def get_campos(self, instance, *args, **kwargs):
            return ''


class Preguntas_frecuentesDetail(Preguntas_frecuentesView, DetailView):
    permission_required = ['detail_preguntasfrecuentes']


class Preguntas_frecuentesCreate(Preguntas_frecuentesView, CreateView):
    permission_required = ['add_preguntasfrecuentes']


class Preguntas_frecuentesUpdate(Preguntas_frecuentesView, UpdateView):
    permission_required = ['change_preguntasfrecuentes']


class Preguntas_frecuentesDelete(Preguntas_frecuentesView, DeleteView):
    permission_required = ['delete_preguntasfrecuentes']

