# Dependencias Django
from django.contrib.auth.models import Group, Permission
from django.http import JsonResponse
from django.core import serializers
from django.utils.text import slugify
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

from ..context_processors import get_conf_menu

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.PerfilesForm import PerfilesForm



class PerfilesView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Group
    form_class = PerfilesForm


class Perfiles(PerfilesView, IndexDatatableView):
    template_name = "perfiles.html"

    permission_required = ['add_group']

    librerias = ['alphanum', 'datatables', 'jstree']

    css = ['Perfiles.css']
    js = ['Perfiles.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'name',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['name']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class PerfilesDetail(PerfilesView, DetailView):
    permission_required = ['change_group']

    def get(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)
        instance = get_object_or_404(self.model.objects, id = id)

        permisos = []
        for permiso in instance.permissions.all():
            permisos.append(permiso.codename)

        data = {
            'id' : instance.id,
            'name' : instance.name,
            'permisos' : permisos
        }

        return JsonResponse(data, safe=False)


class PerfilesCreate(PerfilesView, CreateView):
    permission_required = ['add_group']

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)

        if not form.is_valid():
            return JsonResponse(form.errors, status=422)
        
        instance = Group(name=request.POST['name'])
        instance.save()
        permisos = request.POST['permisos'].split(',')

        for permiso in permisos:
            if permiso[:1] != '#':
                try:
                    permission = Permission.objects.get(codename=permiso)
                    instance.permissions.add(permission)
                except MultipleObjectsReturned as e: 
                    print('Existe duplicado: ', permiso)
                except ObjectDoesNotExist:
                    print('No existe: ', permiso)
        
        return self.response()


class PerfilesUpdate(PerfilesView, UpdateView):
    permission_required = ['change_group']

    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)
        instance = get_object_or_404(self.model.objects, id = id)
        print(instance)
        instance.name=request.POST['name']
        instance.save()
        
        permisos = request.POST['permisos'].split(',')
        
        instance.permissions.clear()

        for permiso in permisos:
            if permiso[:1] != '#':
                try:
                    permission = Permission.objects.get(codename=permiso)
                    instance.permissions.add(permission)
                except MultipleObjectsReturned as e: 
                    print('Existe duplicado: ', permiso)
                except ObjectDoesNotExist:
                    print('No existe: ', permiso)
        
        return self.response()


class PerfilesDelete(PerfilesView, DeleteView):
    permission_required = ['delete_group']


class PerfilesArbol(PerfilesView, DetailView):

    permission_required = ['change_group']

    def get(self, request):
        estructura = self.estructura()
        print(JsonResponse(estructura, safe=False))
        return JsonResponse(estructura, safe=False)


    def estructura(self):
        menus = []
        menu = get_conf_menu()

        for men in menu:
            menus += self._estructura(men, '#todo')

        data = [{
            'id'       : '#todo',
            'text'     : 'Todo',
            'parent'   : '#',
            'icon'     : 'fa fa-sitemap',
            'state'    : {
                'opened' : True
            }
        }] + menus

        return data

    def _estructura(self, menus, padre):
        data = []

        for menu in menus:
            if 'direccion' in menu:
                direccion = '#' + menu['direccion'].replace(':', '-')
            else:
                direccion = "#" + slugify(menu['nombre'])
                
            #menu['direccion'] = menu['direccion'].replace("#", "")

            dato = {
                'id'     : direccion,
                'text'   : menu['nombre'],
                'parent' : padre,
                'icon'   : menu['icono']
            }

            if 'menu' in menu:
                data.extend(self._estructura(menu['menu'], direccion))
            else:
                if 'permisos' in menu:
                    for permiso in menu['permisos']:
                        texto = permiso

                        if 'view' in texto:
                            texto = _('Index')
                        elif 'detail' in texto:
                            texto = _('Detalle')
                        elif 'add' in texto:
                            texto = _('Crear')
                        elif 'change' in texto:
                            texto = _('Actualizar')
                        elif 'delete' in texto:
                            texto = _('Eliminar')
                        
                        data.extend([{
                            'id'     : permiso,
                            'text'   : texto,
                            'parent' : direccion,
                            'icon'   : 'fa fa-code'
                        }])

            data.extend([dato])
        
        return data

