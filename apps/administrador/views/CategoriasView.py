# Dependencias Python
import json  

# Dependencias Django
from django.views.generic.base import TemplateView
from django.http import JsonResponse
from django.core import serializers
from django.utils.text import slugify
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.CategoriasForm import CategoriasForm

# Modelos
from ..models import Categorias


class CategoriasView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Categorias
    form_class = CategoriasForm


class Categorias(CategoriasView, TemplateView):
    template_name = "categorias.html"

    permission_required = ['view_categorias']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'jstree', 'bootstrap-select']

    css = ['Categorias.css']
    js = ['Categorias.js']


class CategoriasCreate(CategoriasView, CreateView):
    ids = {}
    estructura = {}
    permission_required = ['add_categorias']

    def post(self, request, *args, **kwargs):
        estructura = json.loads(request.POST.get('estructura', '{}'))
        
        for ele in estructura:
            ele['id_orig'] = ele['id']
            self.ids[ele['id']] = ele['id']
            self.estructura[ele['id']] = ele

        for ele in self.estructura:
            self.guardar_ele(ele)

        return JsonResponse({
            's':   's',
            'msj': _('Registro Creado Satisfactoriamente')
        })

    def guardar_ele(self, ele):
        if ele['parent'][:1] == 'j':
            ele['parent'] = self.guardar_ele(self.estructura[ele['parent']])

        if ele['id'][:1] == 'j':
            instance = self.model()
        else:
            instance = get_object_or_404(self.model.objects, id = ele['id'])
            
        instance.nombre = ele['text']
        instance.nombre = ele['parent']
        instance.save()
        
        self.ids[ele['id']] = instance.id
        
        return instance.id

class CategoriasArbol(CategoriasView, DetailView):
    
    permission_required = ['change_categorias']

    def get(self, request):
        estructura = self.estructura()
        print(JsonResponse(estructura, safe=False))
        return JsonResponse(estructura, safe=False)


    def estructura(self):
        estructura = self.model.objects.all()
        print(estructura)
        
        data = [{
            'id'       : '0',
            'text'     : 'Categorias',
            'parent'   : '#',
            'icon'     : 'fa fa-sitemap',
            'state'    : {
                'opened' : True
            }
        }]

        return data

    def _estructura(self, menus, padre):
        data = []

        for menu in menus:
            if 'direccion' in menu:
                direccion = '#' + menu['direccion'].replace(':', '-')
            else:
                direccion = "#" + slugify(menu['nombre'])
                
            #menu['direccion'] = menu['direccion'].replace("#", "")

            dato = {
                'id'     : direccion,
                'text'   : menu['nombre'],
                'parent' : padre,
                'icon'   : menu['icono']
            }

            if 'menu' in menu:
                data.extend(self._estructura(menu['menu'], direccion))
            else:
                if 'permisos' in menu:
                    for permiso in menu['permisos']:
                        texto = permiso

                        if 'view' in texto:
                            texto = _('Index')
                        elif 'detail' in texto:
                            texto = _('Detalle')
                        elif 'add' in texto:
                            texto = _('Crear')
                        elif 'change' in texto:
                            texto = _('Actualizar')
                        elif 'delete' in texto:
                            texto = _('Eliminar')
                        
                        data.extend([{
                            'id'     : permiso,
                            'text'   : texto,
                            'parent' : direccion,
                            'icon'   : 'fa fa-code'
                        }])

            data.extend([dato])
        
        return data

