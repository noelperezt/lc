# Dependencias Django
from django.conf import settings
from django.views.generic.base import TemplateView

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
#from ..forms.UbicacionCategoriasForm import UbicacionCategoriasForm

# Modelos
#from ..models import UbicacionCategorias


class UbicacionCategoriasView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    #model = UbicacionCategorias
    #form_class = UbicacionCategoriasForm


class UbicacionCategorias(UbicacionCategoriasView, TemplateView):
    template_name = "ubicacion_categorias.html"

    permission_required = ['view_ubicacion_categorias']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'icheck', 'bootstrap-select']

    css = ['UbicacionCategorias.css']
    js = ['UbicacionCategorias.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'nombre', 'pais', 'ubicacion',
            ('comandos', None, 'get_campos'),
        ]
    }


class UbicacionCategoriasDetail(UbicacionCategoriasView, DetailView):
    permission_required = ['detail_ubicacion_categorias']


class UbicacionCategoriasCreate(UbicacionCategoriasView, CreateView):
    permission_required = ['add_ubicacion-categorias']


class UbicacionCategoriasUpdate(UbicacionCategoriasView, UpdateView):
    permission_required = ['change_ubicacion-categorias']


class UbicacionCategoriasDelete(UbicacionCategoriasView, DeleteView):
    permission_required = ['delete_ubicacion-categorias']

