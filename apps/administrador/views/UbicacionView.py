# Dependencias Django
from django.conf import settings
from django.views.generic.base import TemplateView
from django.http import JsonResponse

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.UbicacionForm import PaisForm, CiudadForm

# Modelos
from ..models import Paises, Ciudades


class UbicacionView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Ciudades
    form_class = CiudadForm


class Ubicacion(UbicacionView, TemplateView):
    template_name = "ubicacion.html"

    permission_required = ['view_ubicacion']

    librerias = ['bootstrap-select']

    css = ['Ubicacion.css']
    js = ['Ubicacion.js']


class UbicacionPaisCreate(UbicacionView, CreateView):
    permission_required = ['add_ubicacion']

    form_class = PaisForm

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)

        if not form.is_valid():
            return JsonResponse(form.errors, status=422)
        
        form.save()

        paises = Paises.objects.all()
        
        resultado = {'_' : ''}

        for pais in paises:
            resultado[pais.id] = pais.pais

        return JsonResponse({
            's' : 's',
            'lista' : resultado
        })

class UbicacionCiudadCreate(UbicacionView, CreateView):
    permission_required = ['add_ubicacion']

    def post(self, request, *args, **kwargs):
        ciudad = Ciudades(pais_id=request.POST.get('pais'), ciudad=request.POST.get('ciudad'))
        ciudad.save()

        pais_id = request.POST.get('pais', '')
        ciudades = Ciudades.objects.filter(pais_id=id).order_by('-ciudad')
        resultado = {'_' : ''}

        for ciudad in ciudades:
            resultado[ciudad.id] = ciudad.ciudad

        return self.response({
            'lista' : resultado
        })

class UbicacionCiudades(UbicacionView, DetailView):
    permission_required = ['detail_-']

    def get(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', '')
        ciudades = Ciudades.objects.filter(pais_id=id).order_by('-ciudad')
        resultado = {'_' : ''}

        for ciudad in ciudades:
            resultado[ciudad.id] = ciudad.ciudad
        
        return JsonResponse({
            'id_ciudad' : resultado
        })