# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable
from apps.datatableview.views.base import DatatableView

# Vista Generica
from ..vista import Vista, VistaMultimedia, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.AlojamientosForm import AlojamientosForm

# Modelos
from apps.alojamiento.models import Inmuebles, Inmuebles_tipo
from ..models import Banners


class AlojamientosView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    #model = Alojamientos
    model = Inmuebles
    form_class = AlojamientosForm


class Alojamientos(AlojamientosView, DatatableView):
    template_name = "alojamientos.html"

    permission_required = ['alojamientos']

    librerias = ['datatables', 'bootstrap-select', 'file-upload', 'jquery-toggle']

    css = ['Alojamientos.css', 'Multimedia.css']
    js = ['Alojamientos.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }

    def get_context_data(self, **kwargs):
        context = super(Alojamientos, self).get_context_data(**kwargs)
        
        context['inmuebles_tipo'] = Inmuebles_tipo.objects.all()
        return context

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class AlojamientosDetail(AlojamientosView, DetailView):
    permission_required = ['alojamientos-detail']


class AlojamientosCreate(AlojamientosView, CreateView):
    permission_required = ['alojamientos-create']


class AlojamientosUpdate(AlojamientosView, UpdateView):
    permission_required = ['alojamientos-update']


class AlojamientosDelete(AlojamientosView, DeleteView):
    permission_required = ['alojamientos-delete']

