# Dependencias Django
from django.conf import settings
from django.core import serializers

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.Quienes_somosForm import Quienes_somosForm

# Modelos
from ..models import QuienesSomos


class Quienes_somosView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = QuienesSomos
    form_class = Quienes_somosForm


class Quienes_somos(Quienes_somosView, IndexDatatableView):
    template_name = "quienes_somos.html"

    permission_required = ['view_quienessomos']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'bootstrap-select', 'tinymce', 'file-upload']

    css = ['Quienes_somos.css', 'Multimedia.css']
    js = ['Quienes_somos.js', 'Multimedia.js']

    def get_context_data(self, **kwargs):
        context = super(Quienes_somos, self).get_context_data(**kwargs) 

        instance = self.model.objects.first()
        if instance is None:
            context['data'] = {}
            context['form'] = self.form_class()
            context['model_id'] = 0
            
        else:
            data = serializers.serialize('python', [instance])

            id = data[0]['pk']
            data = data[0]['fields']
            data['archivo'] = settings.MEDIA_URL + str(data['archivo'])

            context['form'] = self.form_class(initial=data)
            context['data'] = data
            context['model_id'] = id


        return context


class Quienes_somosCreate(Quienes_somosView, CreateView):
    permission_required = ['add_quienessomos']


class Quienes_somosUpdate(Quienes_somosView, UpdateView):
    permission_required = ['change_quienessomos']

