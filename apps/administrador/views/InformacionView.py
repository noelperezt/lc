# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.InformacionForm import InformacionForm

# Modelos
from ..models import Informacion


class InformacionView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Informacion
    form_class = InformacionForm


class Informacion(InformacionView, IndexDatatableView):
    template_name = "informacion.html"

    permission_required = ['view_informacion']

    librerias = ['datatables', 'bootstrap-select']

    css = ['Informacion.css']
    js = ['Informacion.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class InformacionDetail(InformacionView, DetailView):
    permission_required = ['detail_informacion']


class InformacionCreate(InformacionView, CreateView):
    permission_required = ['add_informacion']


class InformacionUpdate(InformacionView, UpdateView):
    permission_required = ['change_informacion']


class InformacionDelete(InformacionView, DeleteView):
    permission_required = ['delete_informacion']

