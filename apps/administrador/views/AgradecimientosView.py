# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.AgradecimientosForm import AgradecimientosForm

# Modelos
from ..models import Agradecimientos


class AgradecimientosView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = Agradecimientos
    form_class = AgradecimientosForm


class Agradecimientos(AgradecimientosView, IndexDatatableView):
    template_name = "agradecimientos.html"

    permission_required = ['view_agradecimientos']

    librerias = ['datatables', 'bootstrap-select', 'tinymce', 'file-upload']

    css = ['Agradecimientos.css', 'Multimedia.css']
    js = ['Agradecimientos.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'titulo', 'descripcion',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['titulo', 'descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class AgradecimientosDetail(AgradecimientosView, DetailView):
    permission_required = ['detail_agradecimientos']

    def get_instance(self, *args, **kwargs):
        instance = super(AgradecimientosDetail, self).get_instance(**kwargs)
        
        #instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class AgradecimientosCreate(AgradecimientosView, CreateView):
    permission_required = ['add_agradecimientos']


class AgradecimientosUpdate(AgradecimientosView, UpdateView):
    permission_required = ['change_agradecimientos']


class AgradecimientosDelete(AgradecimientosView, DeleteView):
    permission_required = ['delete_agradecimientos']

