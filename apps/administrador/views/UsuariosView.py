# Dependencias Django
from django.conf import settings
from django.utils.dateparse import parse_date

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.UsuariosForm import UsuariosForm

# Modelos
from ..models import Usuarios, Paises


class UsuariosView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = Usuarios

    form_class = UsuariosForm


class Usuarios(UsuariosView, IndexDatatableView):
    template_name = "usuarios.html"

    permission_required = ['view_usuarios']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'jstree', 'bootstrap-select', 'bootstrap-datepicker']

    css = ['Usuarios.css']
    js = ['Usuarios.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'username', 
            'email', 
            'first_name', 
            'last_name',
            ('comandos', None, 'get_campos'),
        ]
    }

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['email', 'nombre', 'apellido']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class UsuariosDetail(UsuariosView, DetailView):
    permission_required = ['detail_usuarios']
    serialize_fields = (
        'is_superuser', 'nombre', 'apellido', 'groups',
        'email', 'dni', 'genero', 'nacimiento', 
        'direccion', 'telefono', 'foto', 'pais'
    )

    def get_instance(self, *args, **kwargs):
        instance = super(UsuariosDetail, self).get_instance(**kwargs)
        
        #instance.nacimiento = instance.nacimiento.strftime('%d/%m/%Y')
        instance.foto = settings.MEDIA_URL + str(instance.foto)
        return instance


class UsuariosCreate(UsuariosView, CreateView):
    permission_required = ['add_usuarios']

    def get_post(self, *args, **kwargs):
        data = super(UsuariosCreate, self).get_post(*args, **kwargs)
        data['email'] = data['email'].lower()
        return data


class UsuariosUpdate(UsuariosView, UpdateView):
    permission_required = ['change_usuarios']

    def get_post(self, *args, **kwargs):
        data = super(UsuariosUpdate, self).get_post(*args, **kwargs) 
        data['email'] = data['email'].lower()
        return data


class UsuariosDelete(UsuariosView, DeleteView):
    permission_required = ['delete_usuarios']
