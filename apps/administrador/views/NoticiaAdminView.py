# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable
from apps.datatableview.views.base import DatatableView

# Vista Generica
from ..vista import Vista, VistaMultimedia, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.NoticiaAdminForm import NoticiaAdminForm

# Modelos
#from ..models import NoticiaAdmin
from ..models import Sugerencias #mientras esta listo el modelo de NoticiaAdmin

class NoticiaAdminView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    #model = NoticiaAdmin
    model = Sugerencias
    form_class = NoticiaAdminForm


class NoticiaAdmin(NoticiaAdminView, DatatableView):
    template_name = "noticiaadmin.html"

    permission_required = ['noticiaadmin']

    librerias = ['datatables', 'bootstrap-select', 'file-upload', 'tinymce', 'bootstrap-datepicker']

    css = ['NoticiaAdmin.css', 'Multimedia.css']
    js = ['NoticiaAdmin.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class NoticiaAdminDetail(NoticiaAdminView, DetailView):
    permission_required = ['noticiaadmin-detail']


class NoticiaAdminCreate(NoticiaAdminView, CreateView):
    permission_required = ['noticiaadmin-create']


class NoticiaAdminUpdate(NoticiaAdminView, UpdateView):
    permission_required = ['noticiaadmin-update']


class NoticiaAdminDelete(NoticiaAdminView, DeleteView):
    permission_required = ['noticiaadmin-delete']

