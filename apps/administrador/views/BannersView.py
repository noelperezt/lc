# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, VistaMultimedia, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.BannersForm import BannersForm

# Modelos
from ..models import Banners, Paises, BannersUbicacion


class BannersView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    model = Banners
    form_class = BannersForm


class Banners(BannersView, IndexDatatableView):
    template_name = "banners.html"

    permission_required = ['view_banners']

    librerias = ['alphanum', 'maskedinput', 'datatables', 'bootstrap-select', 'icheck', 'file-upload', 'jquery-toggle']

    css = ['Banners.css', 'Multimedia.css']
    js = ['Banners.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            'titulo', 'posicion', 'activo',
            ('comandos', None, 'get_campos'),
        ]
    }

    def get_context_data(self, **kwargs):
        context = super(Banners, self).get_context_data(**kwargs)
        
        context['form'] = self.form_class
        return context

    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['titulo', 'posicion', 'activo']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class BannersDetail(BannersView, DetailView):
    permission_required = ['detail_banners']

    def get_instance(self, *args, **kwargs):
        instance = super(BannersDetail, self).get_instance(**kwargs)
        
        instance[0].archivo = settings.MEDIA_URL + str(instance[0].archivo)
        return instance


class BannersCreate(BannersView, CreateView):
    permission_required = ['add_banners']


class BannersUpdate(BannersView, UpdateView):
    permission_required = ['change_banners']


class BannersDelete(BannersView, DeleteView):
    permission_required = ['delete_banners']

