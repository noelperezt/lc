# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable

# Vista Generica
from ..vista import Vista, IndexDatatableView, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.OportunidadEmpleoForm import OportunidadEmpleoForm

# Modelos
from ..models import OportunidadEmpleo


class Oportunidad_empleoView(LoginRequiredMixin, PermissionRequiredMixin, Vista):
    template_name = None

    model = OportunidadEmpleo
    form_class = OportunidadEmpleoForm


class Oportunidad_empleo(Oportunidad_empleoView, IndexDatatableView):
    template_name = "oportunidad_empleo.html"

    permission_required = ['view_oportunidadempleo']

    librerias = ['datatables', 'bootstrap-select', 'tinymce']

    css = ['Oportunidad_empleo.css']
    js = ['Oportunidad_empleo.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class Oportunidad_empleoDetail(Oportunidad_empleoView, DetailView):
    permission_required = ['detail_oportunidad_empleo']


class Oportunidad_empleoCreate(Oportunidad_empleoView, CreateView):
    permission_required = ['add_oportunidad_empleo']


class Oportunidad_empleoUpdate(Oportunidad_empleoView, UpdateView):
    permission_required = ['change_oportunidad_empleo']


class Oportunidad_empleoDelete(Oportunidad_empleoView, DeleteView):
    permission_required = ['delete_oportunidad_empleo']

