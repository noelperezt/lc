# Dependencias Django
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from apps.datatableview import columns
from apps.datatableview.datatables import Datatable
from apps.datatableview.views.base import DatatableView

# Vista Generica
from ..vista import Vista, VistaMultimedia, DetailView, CreateView, UpdateView, DeleteView

# Formularios
from ..forms.InstitucionesForm import InstitucionesForm

# Modelos
#from ..models import Instituciones
from ..models import Sugerencias #mientras esta listo el modelo de Instituciones

class InstitucionesView(LoginRequiredMixin, PermissionRequiredMixin, VistaMultimedia):
    template_name = None

    #model = Instituciones
    model = Sugerencias
    form_class = InstitucionesForm


class Instituciones(InstitucionesView, DatatableView):
    template_name = "instituciones.html"

    permission_required = ['instituciones']

    librerias = ['datatables', 'bootstrap-select', 'file-upload', 'smartwizard', 'smartwizard', 'tinymce']

    css = ['Instituciones.css', 'Multimedia.css']
    js = ['Instituciones.js', 'Multimedia.js']

    datatable_options = {
        'structure_template': 'datatableview/bootstrap_structure.html',
        'columns': [
            (_("Pais"), 'pais__pais'), 'descripcion', 
            ('comandos', None, 'get_campos'),
        ]
    }


    class datatable_class(Datatable):
        comandos = columns.TextColumn("Comandos", sources=None, processor='get_campos')

        class Meta:
            columns = ['descripcion']

        def get_campos(self, instance, *args, **kwargs):
            return ''


class InstitucionesDetail(InstitucionesView, DetailView):
    permission_required = ['instituciones-detail']


class InstitucionesCreate(InstitucionesView, CreateView):
    permission_required = ['instituciones-create']


class InstitucionesUpdate(InstitucionesView, UpdateView):
    permission_required = ['instituciones-update']


class InstitucionesDelete(InstitucionesView, DeleteView):
    permission_required = ['instituciones-delete']

