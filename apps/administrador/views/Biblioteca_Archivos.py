from apps.administrador.formulario import FormSubirArchivoBiblioteca
from django.views.generic.edit import CreateView
from apps.administrador.models import Multimedia
from django.core.urlresolvers import reverse_lazy

class SubirArchivo(CreateView):
    model = Multimedia
    form_class =  FormSubirArchivoBiblioteca
    template_name = 'subir_archivo_biblioeteca.html'
    success_url = reverse_lazy('administrador:usuario_listado')