from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from .views.login import IndexView, cerrar_session, Cambiar_claveView, AceptarTerminosView

from .views.AgradecimientosView import Agradecimientos, AgradecimientosDetail, AgradecimientosCreate, AgradecimientosUpdate, AgradecimientosDelete
from .views.BannersView import Banners, BannersDetail, BannersCreate, BannersUpdate, BannersDelete
from .views.CategoriasView import Categorias, CategoriasCreate, CategoriasArbol
from .views.EstudiaTrabajaView import EstudiaTrabaja, EstudiaTrabajaDetail, EstudiaTrabajaCreate, EstudiaTrabajaUpdate, EstudiaTrabajaDelete
from .views.InformacionView import Informacion, InformacionDetail, InformacionCreate, InformacionUpdate, InformacionDelete
from .views.MultimediaView import MultimediaView, MultimediaInfoView, MultimediaListaView
from .views.OficinasView import Oficinas, OficinasDetail, OficinasCreate, OficinasUpdate, OficinasDelete, OficinasCiudades, OficinasMutimedia
from .views.Oportunidad_empleoView import Oportunidad_empleo, Oportunidad_empleoDetail, Oportunidad_empleoCreate, Oportunidad_empleoUpdate, Oportunidad_empleoDelete
from .views.Oportunidad_empleo_postuladosView import Oportunidad_empleo_postulados, Oportunidad_empleo_postuladosDetail, Oportunidad_empleo_postuladosCreate, Oportunidad_empleo_postuladosUpdate, Oportunidad_empleo_postuladosDelete
from .views.PerfilesView import Perfiles, PerfilesDetail, PerfilesCreate, PerfilesUpdate, PerfilesDelete, PerfilesArbol
from .views.Preguntas_frecuentesView import Preguntas_frecuentes, Preguntas_frecuentesDetail, Preguntas_frecuentesCreate, Preguntas_frecuentesUpdate, Preguntas_frecuentesDelete
from .views.Quienes_somosView import Quienes_somos, Quienes_somosCreate, Quienes_somosUpdate
from .views.Responsabilidad_socialView import Responsabilidad_social, Responsabilidad_socialDetail, Responsabilidad_socialCreate, Responsabilidad_socialUpdate, Responsabilidad_socialDelete
from .views.ServiciosView import Servicios, ServiciosDetail, ServiciosCreate, ServiciosUpdate, ServiciosDelete, ServiciosTipo
#from .views.SocialView import Social, SocialDetail, SocialCreate, SocialUpdate, SocialDelete
from .views.TestimoniosView import Testimonios, TestimoniosDetail, TestimoniosCreate, TestimoniosUpdate, TestimoniosDelete
from .views.UbicacionCategoriasView import UbicacionCategorias, UbicacionCategoriasDetail, UbicacionCategoriasCreate, UbicacionCategoriasUpdate, UbicacionCategoriasDelete
from .views.UbicacionView import Ubicacion, UbicacionPaisCreate, UbicacionCiudadCreate, UbicacionCiudades
from .views.UsuariosView import Usuarios, UsuariosDetail, UsuariosCreate, UsuariosUpdate, UsuariosDelete
from .views.SugerenciasView import Sugerencias, SugerenciasDetail, SugerenciasCreate, SugerenciasUpdate, SugerenciasDelete
from .views.AlojamientosView import Alojamientos, AlojamientosDetail, AlojamientosCreate, AlojamientosUpdate, AlojamientosDelete
from .views.EventosView import Eventos, EventosDetail, EventosCreate, EventosUpdate, EventosDelete
from .views.InstitucionesView import Instituciones, InstitucionesDetail, InstitucionesCreate, InstitucionesUpdate, InstitucionesDelete
from .views.NoticiaAdminView import NoticiaAdmin, NoticiaAdminDetail, NoticiaAdminCreate, NoticiaAdminUpdate, NoticiaAdminDelete

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^cerrar_sesion/$', cerrar_session, name='cerrar_sesion'),
    url(r'^cambiar_clave/$', Cambiar_claveView.as_view(), name='cambiar_clave'),

    url(r'^aceptar-terminos/$', AceptarTerminosView.as_view(), name='aceptar-terminos'),

    #usuarios
    url(r'^usuarios/', include([
        url(r'^$',                       Usuarios.as_view(),       name='usuarios'),
        url(r'^detail/(?P<pk>[0-9]+)/$', UsuariosDetail.as_view(), name='usuarios-detail'),
        url(r'^create/$',                UsuariosCreate.as_view(), name='usuarios-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', UsuariosUpdate.as_view(), name='usuarios-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', UsuariosDelete.as_view(), name='usuarios-delete'),
    ])),

    url(r'^perfiles/', include([
        url(r'^$',                       Perfiles.as_view(),       name='perfiles'),
        url(r'^detail/(?P<pk>[0-9]+)/$', PerfilesDetail.as_view(), name='perfiles-detail'),
        url(r'^create/$',                PerfilesCreate.as_view(), name='perfiles-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', PerfilesUpdate.as_view(), name='perfiles-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', PerfilesDelete.as_view(), name='perfiles-delete'),
        url(r'^arbol/$',                 PerfilesArbol.as_view(), name='perfiles-arbol'),
    ])),
    
    url(r'^banners/', include([
        url(r'^$',                       Banners.as_view(),       name='banners'),
        url(r'^detail/(?P<pk>[0-9]+)/$', BannersDetail.as_view(), name='banners-detail'),
        url(r'^create/$',                BannersCreate.as_view(), name='banners-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', BannersUpdate.as_view(), name='banners-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', BannersDelete.as_view(), name='banners-delete'),
    ])),

    #testimonios
    url(r'^testimonios/', include([
        url(r'^$',                       Testimonios.as_view(),       name='testimonios'),
        url(r'^detail/(?P<pk>[0-9]+)/$', TestimoniosDetail.as_view(), name='testimonios-detail'),
        url(r'^create/$',                TestimoniosCreate.as_view(), name='testimonios-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', TestimoniosUpdate.as_view(), name='testimonios-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', TestimoniosDelete.as_view(), name='testimonios-delete'),
    ])),
    
    #estudia_trabaja
    url(r'^estudia-trabaja/', include([
        url(r'^$',                       EstudiaTrabaja.as_view(),       name='estudia-trabaja'),
        url(r'^detail/(?P<pk>[0-9]+)/$', EstudiaTrabajaDetail.as_view(), name='estudia-trabaja-detail'),
        url(r'^create/$',                EstudiaTrabajaCreate.as_view(), name='estudia-trabaja-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', EstudiaTrabajaUpdate.as_view(), name='estudia-trabaja-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', EstudiaTrabajaDelete.as_view(), name='estudia-trabaja-delete'),
        #url(r'^ciudades/(?P<pk>[A-Z]{3})/$', EstudiaTrabajaCiudades.as_view(), name='estudia-trabaja-ciudades'),
    ])),
    
    #ubicacion
    url(r'^ubicacion/', include([
        url(r'^$',                           Ubicacion.as_view(),       name='ubicacion'),
        url(r'^pais/create/$',               UbicacionPaisCreate.as_view(), name='ubicacion-pais-create'),
        url(r'^ciudad/create/$',             UbicacionCiudadCreate.as_view(), name='ubicacion-ciudad-create'),
        url(r'^ciudades/(?P<pk>[A-Z]{3})/$', UbicacionCiudades.as_view(), name='ubicacion-ciudades'),
    ])),

    #categorias
    url(r'^categorias/', include([
        url(r'^$',                       Categorias.as_view(),       name='categorias'),
        url(r'^create/$',                CategoriasCreate.as_view(), name='categorias-create'),
        url(r'^arbol/$',                 CategoriasArbol.as_view(),  name='categorias-arbol'),
    ])),

    #servicios
    url(r'^servicios/', include([
        url(r'^$',                       Servicios.as_view(),       name='servicios'),
        url(r'^detail/(?P<pk>[0-9]+)/$', ServiciosDetail.as_view(), name='servicios-detail'),
        url(r'^create/$',                ServiciosCreate.as_view(), name='servicios-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', ServiciosUpdate.as_view(), name='servicios-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', ServiciosDelete.as_view(), name='servicios-delete'),
        url(r'^add_tipo/$',              ServiciosTipo.as_view(),   name='servicios-tipo'),
    ])),
    
    #Quienes_somos
    url(r'^quienes_somos/', include([
        url(r'^$',                       Quienes_somos.as_view(),       name='quienes_somos'),
        url(r'^create/$',                Quienes_somosCreate.as_view(), name='quienes_somos-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', Quienes_somosUpdate.as_view(), name='quienes_somos-update'),
    ])),
    
    url(r'^responsabilidad_social/', include([
        url(r'^$',                       Responsabilidad_social.as_view(),       name='responsabilidad_social'),
        url(r'^detail/(?P<pk>[0-9]+)/$', Responsabilidad_socialDetail.as_view(), name='responsabilidad_social-detail'),
        url(r'^create/$',                Responsabilidad_socialCreate.as_view(), name='responsabilidad_social-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', Responsabilidad_socialUpdate.as_view(), name='responsabilidad_social-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', Responsabilidad_socialDelete.as_view(), name='responsabilidad_social-delete'),
    ])),

    url(r'^agradecimientos/', include([
        url(r'^$',                       Agradecimientos.as_view(),       name='agradecimientos'),
        url(r'^detail/(?P<pk>[0-9]+)/$', AgradecimientosDetail.as_view(), name='agradecimientos-detail'),
        url(r'^create/$',                AgradecimientosCreate.as_view(), name='agradecimientos-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', AgradecimientosUpdate.as_view(), name='agradecimientos-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', AgradecimientosDelete.as_view(), name='agradecimientos-delete'),
    ])),
    
    url(r'^multimedia/', include([
        url(r'^$',                       MultimediaView.as_view(),      name='multimedia'),
        url(r'^info/$',                  MultimediaInfoView.as_view(),  name='multimedia-info'),
        url(r'^refrescar/$',             MultimediaListaView.as_view(), name='multimedia-refrescar'),
    ])),

    url(r'^oficinas/', include([
        url(r'^$',                           Oficinas.as_view(),       name='oficinas'),
        url(r'^detail/(?P<pk>[0-9]+)/$',     OficinasDetail.as_view(), name='oficinas-detail'),
        url(r'^create/$',                    OficinasCreate.as_view(), name='oficinas-create'),
        url(r'^update/(?P<pk>[0-9]+)/$',     OficinasUpdate.as_view(), name='oficinas-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$',     OficinasDelete.as_view(), name='oficinas-delete'),
        url(r'^ciudades/(?P<pk>[A-Z]{3})/$', OficinasCiudades.as_view(), name='oficinas-ciudades'),
        url(r'^multimedia/$',                OficinasMutimedia.as_view(), name='oficinas-multimedia'),
    ])),

    url(r'^oportunidad_empleo/', include([
        url(r'^$',                       Oportunidad_empleo.as_view(),       name='oportunidad_empleo'),
        url(r'^detail/(?P<pk>[0-9]+)/$', Oportunidad_empleoDetail.as_view(), name='oportunidad_empleo-detail'),
        url(r'^create/$',                Oportunidad_empleoCreate.as_view(), name='oportunidad_empleo-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', Oportunidad_empleoUpdate.as_view(), name='oportunidad_empleo-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', Oportunidad_empleoDelete.as_view(), name='oportunidad_empleo-delete'),
    ])),

    url(r'^oportunidad_empleo_postulados/', include([
        url(r'^$',                       Oportunidad_empleo_postulados.as_view(),       name='oportunidad_empleo_postulados'),
        url(r'^detail/(?P<pk>[0-9]+)/$', Oportunidad_empleo_postuladosDetail.as_view(), name='oportunidad_empleo_postulados-detail'),
        url(r'^create/$',                Oportunidad_empleo_postuladosCreate.as_view(), name='oportunidad_empleo_postulados-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', Oportunidad_empleo_postuladosUpdate.as_view(), name='oportunidad_empleo_postulados-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', Oportunidad_empleo_postuladosDelete.as_view(), name='oportunidad_empleo_postulados-delete'),
    ])),

    url(r'^preguntas_frecuentes/', include([
        url(r'^$',                       Preguntas_frecuentes.as_view(),       name='preguntas_frecuentes'),
        url(r'^detail/(?P<pk>[0-9]+)/$', Preguntas_frecuentesDetail.as_view(), name='preguntas_frecuentes-detail'),
        url(r'^create/$',                Preguntas_frecuentesCreate.as_view(), name='preguntas_frecuentes-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', Preguntas_frecuentesUpdate.as_view(), name='preguntas_frecuentes-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', Preguntas_frecuentesDelete.as_view(), name='preguntas_frecuentes-delete'),
    ])),

    url(r'^informacion/', include([
        url(r'^$',                       Informacion.as_view(),       name='informacion'),
        url(r'^detail/(?P<pk>[0-9]+)/$', InformacionDetail.as_view(), name='informacion-detail'),
        url(r'^create/$',                InformacionCreate.as_view(), name='informacion-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', InformacionUpdate.as_view(), name='informacion-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', InformacionDelete.as_view(), name='informacion-delete'),
    ])),

    url(r'^sugerencias/', include([
        url(r'^$',                       Sugerencias.as_view(),       name='sugerencias'),
        url(r'^detail/(?P<pk>[0-9]+)/$', SugerenciasDetail.as_view(), name='sugerencias-detail'),
        url(r'^create/$',                SugerenciasCreate.as_view(), name='sugerencias-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', SugerenciasUpdate.as_view(), name='sugerencias-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', SugerenciasDelete.as_view(), name='sugerencias-delete'),
    ])),

    url(r'^alojamientos/', include([
        url(r'^$',                       Alojamientos.as_view(),       name='alojamientos'),
        url(r'^detail/(?P<pk>[0-9]+)/$', AlojamientosDetail.as_view(), name='alojamientos-detail'),
        url(r'^create/$',                AlojamientosCreate.as_view(), name='alojamientos-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', AlojamientosUpdate.as_view(), name='alojamientos-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', AlojamientosDelete.as_view(), name='alojamientos-delete'),
    ])),

    url(r'^eventos/', include([
        url(r'^$',                       Eventos.as_view(),       name='eventos'),
        url(r'^detail/(?P<pk>[0-9]+)/$', EventosDetail.as_view(), name='eventos-detail'),
        url(r'^create/$',                EventosCreate.as_view(), name='eventos-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', EventosUpdate.as_view(), name='eventos-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', EventosDelete.as_view(), name='eventos-delete'),
    ])),

    url(r'^instituciones/', include([
        url(r'^$',                       Instituciones.as_view(),       name='instituciones'),
        url(r'^detail/(?P<pk>[0-9]+)/$', InstitucionesDetail.as_view(), name='instituciones-detail'),
        url(r'^create/$',                InstitucionesCreate.as_view(), name='instituciones-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', InstitucionesUpdate.as_view(), name='instituciones-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', InstitucionesDelete.as_view(), name='instituciones-delete'),
    ])),

    url(r'^noticiaadmin/', include([
        url(r'^$',                       NoticiaAdmin.as_view(),       name='noticiaadmin'),
        url(r'^detail/(?P<pk>[0-9]+)/$', NoticiaAdminDetail.as_view(), name='noticiaadmin-detail'),
        url(r'^create/$',                NoticiaAdminCreate.as_view(), name='noticiaadmin-create'),
        url(r'^update/(?P<pk>[0-9]+)/$', NoticiaAdminUpdate.as_view(), name='noticiaadmin-update'),
        url(r'^delete/(?P<pk>[0-9]+)/$', NoticiaAdminDelete.as_view(), name='noticiaadmin-delete'),
    ])),    
]