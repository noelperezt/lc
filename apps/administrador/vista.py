import re
import os
import uuid

from django.core import serializers

from django.core.files.base import ContentFile
from django.db.models import FileField

from django.http import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from LC.settings import STATIC_URL, STATICFILES_DIRS

from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView as DjCreateView, UpdateView as DjUpdateView, DeleteView as DjDeleteView
from django.views.generic.detail import SingleObjectMixin
from apps.datatableview.views.base import DatatableView

from .conf import Conf

from .models import Multimedia

class Vista(object):
    libreriasIniciales = ['OpenSans', 'font-awesome', 'jquery-easing', 'jquery-migrate', 'bootstrap', 'bootbox', 'pace', 'jquery-form', 'blockUI', 'jquery-shortcuts', 'pnotify']
    librerias = []
    multimedia = False

    css = []
    js  = []

    patch_css = (
        'css/',
        'plugins/',
        'apps/administrador/css/',
    )
    patch_js  = (
        'js/',
        'plugins/',
        'apps/administrador/js/',
    )

    msjs = {
        'detail' : _(''),
        'create' : _('Registro Creado Satisfactoriamente'),
        'update' : _('Registro Actualizado Satisfactoriamente'),
        'delete' : _('Registro Eliminado Satisfactoriamente'),
    }

    def get_context_data(self, **kwargs):
        context = super(Vista, self).get_context_data(**kwargs)
        _conf = Conf()
        _librerias = _conf.get_librerias()

        user_agent = self.request.META.get('HTTP_USER_AGENT', '')

        if re.match(r'(?i)msie [5-8]', user_agent, re.I):
            librerias = ['jquery', 'ie']
        else:
            librerias = ['jquery2']

        librerias = librerias + self.libreriasIniciales + self.librerias

        _css = []
        _js  = []
        css  = []
        js   = []
        for libreria in librerias:
            if 'js' in _librerias[libreria]:
                for archivo in _librerias[libreria]['js']:
                    _js.append(archivo)
            if 'css' in _librerias[libreria]:
                for archivo in _librerias[libreria]['css']:
                    _css.append(archivo)

        _css += self.css
        _js  += self.js

        for archivo in _css:
            if re.match(r'^http', archivo, re.I):
                css.append(archivo)
            else:
                for patch in self.patch_css:
                    for static_dir in STATICFILES_DIRS:
                        ruta = (static_dir + '/' + patch + archivo).replace('\\', os.sep).replace('/', os.sep)

                        if os.path.isfile(ruta):
                            css.append(STATIC_URL + patch + archivo)
                            break

        for archivo in _js:
            if re.match(r'^http', archivo, re.I):
                js.append(archivo)
            else:
                for patch in self.patch_js:
                    for static_dir in STATICFILES_DIRS:
                        ruta = (static_dir + '/' + patch + archivo).replace('\\', os.sep).replace('/', os.sep)

                        if os.path.isfile(ruta):
                            js.append(STATIC_URL + patch + archivo)
                            break

        context['js'] = js
        context['css'] = css
        if "form_class" in dir(self):
            context['form'] = self.form_class(self.request.GET)

        return context

    def response(self, data=None, status=200):
        if self.request.is_ajax():
            return JsonResponse(self.get_data_response(data), status=status)
        else:
            return data


    def form_invalid(self, form):
        response = super(Vista, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=422) 
        else:
            return form.errors


    def form_valid(self, form):
        response = super(Vista, self).form_valid(form)
        return self.response()


    def get_msj(self):
        return self.msjs[self.action]


    def merge_dicts(self, *dict_args):
        result = {}
        for dictionary in dict_args:
            result.update(dictionary)
        return result
        

    def get_data_response(self, data):
        _data = {
            's': 's',
            'msj' : self.get_msj()
        }
        #print(data)
        if data is not None:
            _data = self.merge_dicts(_data, data)

        return _data

    def procesar_archivos(self, archivo, *args, **kwargs):
        ruta = kwargs.get('ruta', '');
        en_media = kwargs.get('en_media', True);
        nombre_origen = kwargs.get('nombre_origen', False);

        if (archivo in self.request.FILES):
            archivo = self.request.FILES[archivo]
        else:
            return ''
            
        archivo_nombre = archivo.name
        if nombre_origen == False:
            archivo_nombre = str(uuid.uuid4().hex) + '.' + archivo_nombre.lower().split('.')[-1]

        if en_media:
            ruta = os.path.join(settings.MEDIA_ROOT, ruta)

        try:
            if not os.path.exists(ruta):
                os.makedirs(ruta, exist_ok=True)
        except OSError as e:
            if e.errno == 17:  #el directorio ya existe
                pass
            else:
                print(e)

        file_obj = open(os.path.join(ruta, archivo_nombre), 'wb+')
        contenido = ContentFile(archivo.read())

        for chunk in contenido.chunks():
            file_obj.write(chunk)
        
        file_obj.close()

        regex = re.compile(r"/+", re.IGNORECASE)

        salida = settings.MEDIA_URL + '/' + os.path.join(ruta, archivo_nombre).replace(settings.MEDIA_ROOT, '')
        salida = salida.replace('\\', '/')
        salida = regex.sub('/', salida)

        return salida

class VistaMultimedia(Vista):
    
    multimedia = True

    def get_context_data(self, **kwargs):
        context = super(VistaMultimedia, self).get_context_data(**kwargs)

        context['multimedia'] = Multimedia.objects.all().order_by('-id')[:30]

        return context
    

class IndexView(TemplateView):

    def get(self, request, *args, **kwargs):

        if request.user.activo:
            return super(IndexView, self).get(request, *args, **kwargs)
        
        return redirect('administrador:aceptar_terminos')


class IndexDatatableView(DatatableView):

    def get(self, request, *args, **kwargs):

        if request.user.activo:
            return super(IndexDatatableView, self).get(request, *args, **kwargs)
        
        return redirect('administrador:aceptar_terminos')


class BaseViewMixin(object): 

    def get_get(self, *args, **kwargs):
        return self.request.GET.copy()

    def get_post(self, *args, **kwargs):
        return self.request.POST.copy()
    
    def antes_validar(self, form, *args, **kwargs):
        pass
    
    def antes_guardar(self, form, *args, **kwargs):
        pass


class DetailView(SingleObjectMixin, View, BaseViewMixin):
    action = 'detail'
    serialize_fields = '__ALL__'
    remove_fields = ('created_at', 'updated_at',)

    def get(self, request, *args, **kwargs):
        instance = self.get_instance()

        multimedia = []
        if self.multimedia:
            for archivo in instance.multimedia.all():
                multimedia.append(Multimedia.objects.get(archivo))

        if self.serialize_fields == '__ALL__':
            data = serializers.serialize('python', [instance])
        else:
            data = serializers.serialize('python', [instance], fields=self.serialize_fields)

        data = self.procesar_serialize(data)
        for field_name in self.remove_fields:
            if field_name in data:
                data.pop(field_name)

        return JsonResponse(data, safe=False)

    def procesar_serialize(self, data, *args, **kwargs):
        return data[0]['fields']

    def get_instance(self, *args, **kwargs):
        id = self.kwargs.get('pk', 0)
         
        return get_object_or_404(self.model.objects, id = id)


class CreateView(DjCreateView, BaseViewMixin):
    action = 'create'

    def post(self, request, *args, **kwargs):
        form = self.form_class(self.get_post(), request.FILES)
        
        self.antes_validar(form)
        if not form.is_valid():
            return JsonResponse(form.errors, status=422)
        
        self.antes_guardar(form)
        instance = form.save()

        if self.multimedia:
            instance.multimedia.clear() 
            for ids in request.POST.getlist('id_multimedia[]', []):
                instance.multimedia.add(ids)

        return self.response()


class UpdateView(DjUpdateView, BaseViewMixin):
    action = 'update'

    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)

        instance = get_object_or_404(self.model.objects, id = id)
        form = self.form_class(self.get_post(), request.FILES, instance = instance)
        print(self.get_post()) 
        self.antes_validar(form, instance)
        
        if not form.is_valid():
            return JsonResponse(form.errors, status=422)

        self.antes_guardar(form, instance)

        instance = form.save()

        if self.multimedia:
            instance.multimedia.clear() 
            for ids in request.POST.getlist('id_multimedia[]', []):
                instance.multimedia.add(ids)

        return self.response()


class DeleteView(DjDeleteView, BaseViewMixin):
    action = 'delete'

    def delete(self, request, *args, **kwargs):
        id = self.kwargs.get('pk', 0)

        instance = get_object_or_404(self.model.objects, id = id)
        instance.delete()

        return self.response()
