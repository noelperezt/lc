# Dependencias Django
from django.shortcuts import render,redirect,HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login as login_auth, logout
from django.contrib import messages
from django.views.generic.base import TemplateView

# Vista Generica
from apps.administrador.vista import Vista

class IndexView(Vista, TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated() != True:
            self.template_name = "login.html"
                

        return super(IndexView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.login(request):
            if request.GET:
                return HttpResponseRedirect(request.GET.get('next'))
            else:
                self.template_name = "index.html"
        else:
            self.template_name = "login.html"

        return super(IndexView, self).get(request, *args, **kwargs)

    def login(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(email=username, password=password)
        if user is not None:
            if user.is_active:
                login_auth(request, user)
                return True
            else:
                messages.add_message(request, messages.INFO, _('Cuenta de usuario inactiva'))
                return False
        else:
            messages.add_message(request, messages.INFO, _('Nombre de usuario o contraseña no válido'))


def cerrar_session(request):
    logout(request)
    return redirect('/administrador/')

def cambiar_clave(request):
    return render(request, 'cambiar_clave.html')