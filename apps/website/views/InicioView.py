from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views import View


class Inicio(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return render(request, 'base.html')
        
        return render(request, 'inicio_sesion.html')

    def post(self, request, *args, **kwargs):       
        if self.iniciar_sesion(request):
            if request.GET:
                return HttpResponseRedirect(request.GET.get('next'))
            else:
                return render(request, 'base.html')
        else:
            return render(request, 'inicio_sesion.html')
        #if request.is_ajax:                        
            #if request.POST.has_key('comment'):
                    #process comment

            #if request.POST.has_key('vote'):
                    #process vote

    def iniciar_sesion(request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return True
            else:
                messages.add_message(request, messages.INFO,
                                     _('Cuenta de usuario inactiva'))
                return False
        else:
            messages.add_message(request, messages.INFO, _(
                'Nombre de usuario o contraseña no válido'))