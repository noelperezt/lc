# Dependencias Django
from django.conf import settings

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin

from django.views.generic.base import TemplateView

# Vista Generica
from ..vista import Vista, DetailView, CreateView, UpdateView, DeleteView

# Formularios
#from ..forms.HomeForm import HomeForm

# Modelos
#from ..models import Usuarios


class HomeView(Vista, TemplateView):
    template_name = "home.html"