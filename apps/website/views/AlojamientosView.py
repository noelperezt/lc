# Dependencias Django
import json

# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin
from django.conf import settings
from django.http import HttpResponseBadRequest, JsonResponse
from django.views.generic.base import TemplateView

from apps.administrador.models import Ciudades, Paises, Servicios
from apps.alojamiento.models import Inmuebles_tipo

# Vista Generica
from ..vista import CreateView, DeleteView, DetailView, UpdateView, Vista


class AlojamientosView(Vista, TemplateView):
    template_name = "website_alojamientos.html"
    js = ['website_alojamientos.js']

    def get_context_data(self, **kwargs):
        """Modificar el contexto"""
        context = super(AlojamientosView, self).get_context_data(**kwargs)
        context['tipos_alojamientos'] = Inmuebles_tipo.objects.values('id', 'descripcion').all()
        context['servicios'] = Servicios.objects.all()
        context['paises'] = Paises.objects.values('id', 'pais').all()
        context['ciudades'] = Ciudades.objects.values('ciudad').all()[:10]
        context['precios'] = zip(list(range(0, 100000, 1000)),list(range(0, 100000, 1000)))
        return context


def get_cities_by_countryid(request):
    """Obtiene una lista de ciudades dado un país"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest(request)

    return JsonResponse(
        [
            dict(item)
            for item in Ciudades.objects.filter(pais=request.POST["pais"])
            .values('id','ciudad')
        ],
        safe=False)


class AlojamientosCategorias(Vista, TemplateView):
    template_name = "alojamientos_categorias.html"
    paginate_by = 20

    def get_queryset(self):
        tipo = self.request.POST.get('tipo_alojamiento', None)
        precio = self.request.POST.get('precio', None)
        servicio = self.request.POST.get('servicio', None)
        pais = self.request.POST.get('ciudad', None)
        ciudad = self.request.POST.get('ciudad', None)
        #queryset = Servicios.objects.prefetch_related()

    def get_context_data(self, **kwargs):
        """Modificar el contexto"""
        context = super(AlojamientosCategorias, self).get_context_data(**kwargs)
        context['tipos_alojamientos'] = {'id':'uno','nombre':'servicio1'},{'id':'dos','nombre':'servicio2'},{'id':'tres','nombre':'servicio3'}#ModeloTiposAlojamientos.objects.values('id','descripcion').all()
        context['servicios'] = Servicios.objects.all()
        context['paises'] = Paises.objects.values('id', 'pais').all()
        context['ciudades'] = Ciudades.objects.values('ciudad').all()
        context['precios'] = list(range(0, 100000, 1000))
        return context

class AlojamientosDetalle(Vista, TemplateView):
    template_name = "alojamientos_detalle.html"
    js = ['alojamientos_detalle.js']

    # queryset = Servicios.objects.select_related().get(self.kwargs['pk'])

    def queryset(self):
        pass
