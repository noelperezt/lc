# Dependencias Django
from apps.administrador.models import Servicios
# Dependencias
from braces.views import LoginRequiredMixin, PermissionRequiredMixin
from django.conf import settings
from django.views.generic.base import TemplateView

# Vista Generica
from ..vista import CreateView, DeleteView, DetailView, UpdateView, Vista


class InstitucionView(Vista, TemplateView):
    template_name = "website_instituciones.html"


class InstitucionDetalle(Vista, TemplateView):
    template_name = "institucion_detalle.html"
    js = ['institucion_detalle.js']

