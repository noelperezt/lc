import os
import importlib
from django.conf import settings

def menu(request):
    return {'MENU': get_conf_menu()}


def get_conf_menu():
    MENU_DATA = []

    for app in settings.INSTALLED_APPS:
        if app[:6] == 'django':
            continue
        
        arg = app.split('.')
        arg += ['settings', 'menu.py']
        
        argFile = [settings.BASE_DIR]
        argFile += arg
        local_settings = os.path.join(*argFile)
        
        if os.path.isfile(local_settings):
            importFile = '.'.join(arg)[0:-3]
            i = importlib.import_module(importFile)
            MENU_DATA.append(i.MENU)

    return MENU_DATA
