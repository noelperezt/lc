from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from ..models import Usuarios

        
class UsuariosForm(forms.ModelForm):

    password = forms.CharField(label=_("Password"), required=False, widget=forms.PasswordInput)
    nacimiento = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for campo in self.fields:
            self.fields[campo].widget.attrs.update({
                'class' : 'form-control campo',
                'placeholder' : self.fields[campo].label
            })

        self.fields['pais'].widget.attrs.update({'class' : 'form-control campo selectpicker'})
        self.fields['genero'].widget.attrs.update({'class' : 'form-control campo selectpicker'})


    def save(self, commit=True):
        user = super(UsuariosForm, self).save(commit=False)
        password = self.cleaned_data["password"]
        
        if user.pk:
             if password != '':
                 user.set_password(password)
        else:
             user.set_password(password)

        if commit:
            user.save()

        return user


    class Meta:
        model = Usuarios
        fields = [
            'nombre', 
            'apellido', 
            'email', 
            'password', 
            'dni', 
            'genero', 
            'nacimiento', 
            'direccion', 
            'telefono', 
            'foto', 
            'pais', 
            'is_superuser'
        ]
        #localized_fields = ('nacimiento',)
        widgets = {
            'direccion': forms.Textarea(attrs={'class': 'form-control campo', 'rows': 5, 'cols': 200, 'style': 'resize:none;'})
        }