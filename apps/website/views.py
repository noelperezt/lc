from django.shortcuts import render,redirect,HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse
# Create your views here.

def index(request):
    if request.user.is_authenticated():
        return render(request, 'base.html')
    else:
        if request.method == 'POST':
            if iniciar_sesion(request):
                if request.GET:
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return render(request, 'base.html')
            else:
                return render(request, 'inicio_sesion.html')

    return render(request, 'inicio_sesion.html')

def iniciar_sesion(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return True
        else:
            messages.add_message(request, messages.INFO,
                                 _('Cuenta de usuario inactiva'))
            return False
    else:
        messages.add_message(request, messages.INFO, _(
            'Nombre de usuario o contraseña no válido'))

def crear_usuario(request):
    return render(request, 'usuarios.html')

def banners(request):
    return render(request, 'banners.html')

def testimonios(request):
    return render(request, 'testimonios.html')

def estudia_trabaja(request):
    return render(request, 'estudia_trabaja.html')

def ubicacion_categorias(request):
    return render(request, 'ubicacion_categorias.html')

def servicios(request):
    return render(request, 'servicios.html')
def cerrar_sesion(request):
    logout(request)
    return redirect('/administrador/')

