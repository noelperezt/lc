from django import forms

class formulario(forms.ModelForm):
    INPUT_FORMAT = (
        ('%d/%m/%Y','%m/%d/%Y')
    )
    # fecha = forms.DateField(input_formats = INPUT_FORMAT)
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            self.fields[campo].widget.attrs.update({'class' : 'form-control'})
        
        