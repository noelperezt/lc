from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required

from .views.login import IndexView, cerrar_session, cambiar_clave
from .views.HomeView import HomeView
from .views.AlojamientosView import AlojamientosView, AlojamientosCategorias, AlojamientosDetalle, get_cities_by_countryid
from .views.NoticiasView import NoticiasView
from .views.EstudiaTrabajaView import EstudiaTrabajaView
from .views.InstitucionView import InstitucionView, InstitucionDetalle

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^cerrar_sesion/$', cerrar_session, name='cerrar_sesion'),
    url(r'^cambiar_clave/$', cambiar_clave, name='cambiar_clave'),

    #alojamientos
    url(r'^alojamientos/',
        include([
            url(r'^$', AlojamientosView.as_view(), name='alojamientos'),
            url(
                r'^categorias$',
                AlojamientosCategorias.as_view(),
            name='alojamientos-categorias'),
            url(
                r'^detail/(?P<pk>[0-9]+)/$',
                AlojamientosDetalle.as_view(),
                name='alojamientos-detail'),
            url(
                r'^get_cities/$',
                get_cities_by_countryid,
                name='obtener-ciudades'),
        ])
    ),
    #noticias
    url(r'^noticias/', NoticiasView.as_view(), name='noticias'),
    url(r'^estudia_trabaja/', EstudiaTrabajaView.as_view(), name='estudia_trabaja'),
    #instituciones
    url(r'^instituciones/',
        include([
            url(r'^$', InstitucionView.as_view(), name='instituciones'),
            url(
            r'^detail/(?P<pk>[0-9]+)/$',
            InstitucionDetalle.as_view(),
            name='instituciones-detail')
        ])
    ),
]