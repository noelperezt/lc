import os
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

class Modelo(models.Model):

    def path_and_rename(self, path, instance, filename):
        file_root, file_ext = os.path.splitext(filename)
        filename = '{}{}'.format(get_random_string(20).lower(), file_ext)
        
        return os.path.join(path, filename)

    class Meta:
        abstract            = True
        get_latest_by       = 'updated_at'
        ordering            = ('-updated_at', '-created_at',)
        default_permissions = ('add', 'change', 'delete', 'view')