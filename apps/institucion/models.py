from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.administrador.models import Ciudades, Multimedia, Servicios

# Create your models here.


class Cursos_categorias(models.Model):

    descripcion = models.CharField(
        max_length=100, verbose_name=_('Descripción'))
    id_categoria_padre = models.ForeignKey(
        'self',
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name=_('categoría padre'))
    activo = models.BooleanField(default=False, verbose_name=_('Descripción'))

    created_at = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'cursos_categorias'
        verbose_name = _('Curso Categoria')
        verbose_name_plural = _('Cursos Categorias')

        permissions = (("view_cursos_categorias", "Can view Curso Categoria"),
                       ("detail_cursos_categorias",
                        "Can detail Curso Categoria"), )


class Cursos(models.Model):
    nombre_curso = models.CharField(max_length=60, verbose_name=_('Nombre del curso'))
    activo = models.BooleanField(default=False, verbose_name=_('Activo'))
    categoria_curso = models.ForeignKey(Cursos_categorias, on_delete=models.CASCADE, verbose_name=_('Curso'))
    semestres = models.IntegerField(default=0, null=False, blank=False, verbose_name=_('Semestres'))

    created_at = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'cursos'
        verbose_name = _('Curso')
        verbose_name_plural = _('Cursos')

        permissions = (("view_cursos", "Can view Curso"),
                       ("detail_cursos", "Can detail Curso"), )


class Institucion(models.Model):

    ESTADO = (('G', _('Datos Generales')), ('S', _('Cargó sedes')),
              ('A', _('Asoció cursos a las sedes')), ('F', _('Fin')))

    nombre = models.CharField(
        max_length=80, null=False, blank=False, verbose_name=_('Nombre'))
    logo = models.ForeignKey(
        Multimedia, on_delete=models.PROTECT, verbose_name=_('Logo'))
    portada = models.ForeignKey(
        Multimedia,
        related_name='archivo_portada',
        on_delete=models.PROTECT,
        verbose_name=_('Portada'))
    catalogo = models.FileField(
        upload_to='catalogo/', verbose_name=_('Catalogo'))
    descripcion = models.TextField(
        null=False, blank=False, verbose_name=_('Descripción'))
    privado = models.BooleanField(default=True, verbose_name=_('Privado'))
    estado_carga = models.CharField(
        max_length=1,
        choices=ESTADO,
        default='G',
        verbose_name=_('Estado Carga'))

    created_at = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'institucion'
        verbose_name = _('Institución')
        verbose_name_plural = _('Instituciones')

        permissions = (("view_institucion", "Can view Institución"),
                       ("detail_institucion", "Can detail Institución"), )


class Sedes(models.Model):

    nombre = models.CharField(max_length=60, verbose_name=_('Nombre'))
    direccion = models.TextField(verbose_name=_('Dirección'))
    ciudad = models.ForeignKey(
        Ciudades, on_delete=models.PROTECT, verbose_name=_('Ciudad'))
    institucion = models.ForeignKey(
        Institucion, on_delete=models.PROTECT, verbose_name=_('Institución'))
    edad_minima = models.IntegerField(verbose_name=_('Edad Minima'))
    principal = models.BooleanField(default=False, verbose_name=_('Principal'))
    cantidad_salon = models.IntegerField(verbose_name=_('Cantidad Salon'))
    alumno_salon = models.IntegerField(verbose_name=_('Alumno Salon'))
    telefono = models.IntegerField(verbose_name=_('Telefono'))
    correo = models.EmailField(
        max_length=80,
        null=False,
        blank=False,
        db_index=True,
        unique=True,
        verbose_name=_('Correo'))
    iframe_ubicacion = models.CharField(
        max_length=200,
        null=False,
        blank=False,
        verbose_name=_('Iframe ubicación'))
    cursos = models.ManyToManyField(Cursos, verbose_name=_('Cursos'))
    servicios = models.ManyToManyField(Servicios, verbose_name=_('Servicios'))
    archivos = models.ManyToManyField(Multimedia)
    created_at = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'sedes'
        verbose_name = _('Sede')
        verbose_name_plural = _('Sedes')

        permissions = (("view_sedes", "Can view Sede"), ("detail_sedes",
                                                         "Can detail Sede"), )

class Cursos_sedes(models.Model):
    descripcion = models.TextField(verbose_name=_('Descripción'))    
    
    curso = models.ForeignKey(Cursos)
    sede = models.ForeignKey(Sedes)

    
class Cursos_intensidad(models.Model):
    descripcion = models.TextField(verbose_name=_('Descripción'))
    hora_semana = models.CharField(max_length=30, verbose_name=_('Horas de clase semanal'))
    duracion = models.IntegerField(default=True, null=True, verbose_name=_('Duración'))
    activo = models.BooleanField(default=False, verbose_name=_('Activo'))
    cantidad_alumno = models.IntegerField(verbose_name=_('Cantidad de Alumnos'))

    monto_lc = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Monto Total LC'))
    monto_inst = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Monto Total'))
    porcentaje = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name=_('Monto Total'))
    curso_sede = models.ForeignKey(Cursos_sedes, on_delete = models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(auto_now=True,editable=False,verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'cursos_intensidad'
        verbose_name = _('Cursos Intensidad')
        verbose_name_plural = _('Cursos Intensidad')

        permissions = (("view_cursos_intensidad",
                        "Can view Cursos Intensidad"),
                       ("detail_cursos_intensidad",
                        "Can detail Cursos Intensidad"), )

class Cursos_fechas(models.Model):
    fecha_inicio = models.DateTimeField(null=False, blank=False)
    fecha_fin = models.DateTimeField(null=False, blank=False)
    curso_intensidad = models.ForeignKey(Cursos_intensidad, on_delete = models.CASCADE)


class Cursos_material(models.Model):

    curso = models.ForeignKey(
        Cursos, on_delete=models.CASCADE, verbose_name=_('Curso'))
    material_apoyo = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_('Material de Apoyo'))

    created_at = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Creación'))
    update_at = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name=_('Fecha de Actualización'))

    class Meta:
        db_table = 'cursos_material'
        verbose_name = _('Cursos Material')
        verbose_name_plural = _('Cursos Materiales')

        permissions = (("view_cursos_material", "Can view Cursos Material"),
                       ("detail_cursos_material",
                        "Can detail Cursos Material"), )
